﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HomeButton : MonoBehaviour {

	public GameObject convergenceSpace;

	private float distance;
	private Vector2 myPosition2D;
	private Vector2 reticlePosition2D;
	private float magicBoxNumber;
	private float scaler;


	private float MAXSCALE = 1.0f;
	private float MINSCALE = 0.0f;
	private float DECAYRATE = 2.2f;
	private float CLAMPMAX = 1.0f;
	private float CLAMPMIN = 0.0f;

	void Update(){


		this.transform.position = new Vector3 (0.0f, this.transform.position.y, 8.0f);
		//print ("button position: " + this.transform.position);
		this.transform.localRotation = new Quaternion (-convergenceSpace.transform.rotation.x, -convergenceSpace.transform.rotation.y, convergenceSpace.transform.rotation.z, convergenceSpace.transform.rotation.w);

		magicBoxNumber = DynamicScaler(GetDistance());
		//print ("magicbox: " + magicBoxNumber);
		//this.GetComponent<CanvasGroup> ().alpha = magicBoxNumber;
		//this.gameObject.GetComponent<Image> ().color = new Color (255.0f, 255.0f, 255.0f, magicBoxNumber);


	}


		public float DynamicScaler(float dist){

			scaler = MAXSCALE * Mathf.Exp (-DECAYRATE * dist); 
			scaler = Mathf.Clamp (scaler, CLAMPMIN, CLAMPMAX);

			return scaler;

		}


		public float GetDistance(){

		myPosition2D = new Vector2 (this.transform.position.x, this.transform.position.y);
		return Vector2.Distance (Vector3.zero, myPosition2D);

		}

		
		public float GetMagicBox(){
			return magicBoxNumber;

		}
		



}
