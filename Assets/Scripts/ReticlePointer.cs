﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReticlePointer : MonoBehaviour {

	private Vector3 buttonPos;
	private Vector3 buttonLocalPos;
	private GameObject pastButton;
	private GameObject button;
	private Vector3 buttonOrigin;
	private Vector3 buttonLocalOrigin;
	private bool hasPosition;

	public Vector3 target;
	public float speed;
	public float stepTo;
	public GameObject reticle;




	//private Vector3 targetOrigin;
	
	void Start () {
		stepTo = 0.0f;
		target = new Vector3(0, 0, 0);
		speed = .5f;
		buttonOrigin = new Vector3 (999, 999, 999);
		buttonLocalOrigin = new Vector3 (999, 999, 999);
		hasPosition = false;
	}

	void Update(){

		button = GameObject.Find("Button");
		if (button != null) {
			buttonPos = button.transform.position;
			buttonLocalPos = button.transform.localPosition;

			// avoids null pointer on first loop
			if(pastButton == null){
				pastButton = button;
			}
			if (pastButton.GetInstanceID () != button.GetInstanceID ()) {
				stepTo = 0;
			}
			Debug.Log ("ButtonVector: " + buttonPos);
			float xx = buttonPos.x;
			float yy = buttonPos.y;
			Debug.Log ("ButtonX: " + xx);
			Debug.Log ("ButtonY: " + yy);

			if (xx <= 0.5f && xx >= -0.5f && yy <= 0.5f && yy >= -0.5f && stepTo < 1) {

				if (stepTo == 0) {
					buttonOrigin = buttonPos;
					buttonLocalOrigin = buttonLocalPos;
					target.z = buttonOrigin.z;

				}

				stepTo += speed * Time.deltaTime;
				Debug.Log ("Step is " + stepTo);
				button.transform.position = Vector3.Lerp (buttonOrigin, target, stepTo);


				if (stepTo > 1) {
					stepTo = 0;

				}

				hasPosition = true;

			}

//			if ((xx >= 0.5f || xx <= -0.5f || yy >= 0.5f || yy <= -0.5f) && hasPosition == true){
//				print ("Out of the Zone");
//				//pastButton = button;
//				//button.transform.position = Vector3.Lerp (buttonPos, buttonOrigin, stepTo);
//
//				button.transform.Translate(buttonLocalOrigin);
//			}


			pastButton = button;
		}
	}


}
