﻿/****************************************************************************************************************************************
 * © 2016 Daqri International. All Rights Reserved.                                                                                    	*
 *                                                                                                                                     	*
 *     NOTICE:  All software code and related information contained herein is, and remains the property of DAQRI INTERNATIONAL and its 	*
 * suppliers, if any.  The intellectual and technical concepts contained herein are proprietary to DAQRI INTERNATIONAL and its         	*
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and/or trade secret law, and the expression of			*
 * those concepts is protected by copyright law. Dissemination, reproduction, modification, public display, reverse engineering, or 	*
 * decompiling of this material is strictly forbidden unless prior written permission is obtained from DAQRI INTERNATIONAL.            	*
 *																																		*
 *																																		*
 *																																		*
 *     Created by:        	<full name> - <email address>																				*                                
 *     Created on:        	<month> <day> <year>																						*                
 *     File Purpose:    	<summary>																									*                                                                                    
 *																																		*
 *     Guide:               <how to use>																								*
 *																																		*
 ****************************************************************************************************************************************/
using UnityEngine;
using System.Collections;

public class DEV_fakeImu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		this.transform.eulerAngles += new Vector3 (0.0f, 0.05f, 0.0f);
	
	}
}
