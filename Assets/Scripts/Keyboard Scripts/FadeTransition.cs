﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeTransition : MonoBehaviour {


	private bool isHalf;

	void Start(){
		
		isHalf = true;

	}

	void Update(){
		
		if (isHalf) {
			this.GetComponent<Image> ().color = new Vector4 (this.GetComponent<Image> ().color.r, this.GetComponent<Image> ().color.g, this.GetComponent<Image> ().color.b, 0.5f);
		} else if (!isHalf) {
			this.GetComponent<Image> ().color = new Vector4 (this.GetComponent<Image> ().color.r, this.GetComponent<Image> ().color.g, this.GetComponent<Image> ().color.b, 1.0f);
		} 
			
	}

	public void SetIsHalf(bool input){
		
		isHalf = input;

	}



}
