﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextManager : MonoBehaviour {


	private Text[] allButtons;
	private string letter;
	private string[] allLetters;
	private bool isCapital;
	private string output;

	public GameObject keyboard;
	public GameObject titleText;
	public GameObject results;
	public GameObject spaceBar;
	public GameObject submit;
	public GameObject shiftOn;
	public GameObject shiftOff;
	public GameObject clear;

	void Start(){
		isCapital = true;
		allButtons = keyboard.GetComponentsInChildren<Text> ();
		allLetters = new string[allButtons.Length];
		for (int i = 0; i < allButtons.Length; i++) {
			allLetters [i] = allButtons [i].text;
		}


	}

	void Update(){

		if (isCapital) {
			//shiftOn.SetActive(true);
			//shiftOff.SetActive(false);
			for (int i = 0; i < allLetters.Length; i++) {
				allLetters [i] = allLetters [i].ToUpper();
				allButtons [i].text = allLetters [i];
			}


		}else if (!isCapital) {
			shiftOn.SetActive(false);
			shiftOff.SetActive(true);
			for (int i = 0; i < allLetters.Length; i++) {
				allLetters [i] = allLetters [i].ToLower();
				allButtons [i].text = allLetters [i];
			}
		}

		titleText.GetComponent<Text> ().text = output;

		results.GetComponent<Text> ().text =  "The entered IP address is: " + output;

		spaceBar.GetComponentInChildren<Text> ().text = "Space";
		clear.GetComponentInChildren<Text> ().text = "Clear";
		submit.GetComponentInChildren<Text> ().text = "SUBMIT";



	}

	public void switchIsCapital(){
		if (isCapital == false) {
			isCapital = true;
		} else if (isCapital == true) {
			isCapital = false;
		}
	}

	public void setIsCapital(bool input){
		isCapital = input;
	}


	public void QuitApplication(){
		Application.Quit();
	}

	public void ClearText(){
		output = "";
	}

	public void BackSpace(){
		output = output.Remove(output.Length - 1);
	}

	public void TypeText(GameObject input){
		
		letter = input.GetComponentInChildren<Text> ().text;

		output = output + letter;
	}

	public void typeSpace(){
		output = output + " ";
	}

	public string GetLetter(){
		return letter;
	}










}
