﻿using UnityEngine;
using System.Collections;

public class PinButton : MonoBehaviour {

	//TODO: Total refactor

	public Multispatial switcher;
	public string space = "HUDSpace";

	void Start() {

		if (switcher == null) {
			//Search through parents for the multispatial component
			GameObject parent = transform.parent.gameObject;
			while(parent != parent.transform.root) {
				if(parent.GetComponent<Multispatial>()) {
					switcher = parent.GetComponent<Multispatial>();
					break;
				} else {
					parent = parent.transform.parent.gameObject;
				}
			}
		}
	}

	//Interesting - Note that this can be called, even when the component is inactive.
	public void OnActivateButton() {
		if (switcher) {
			if(switcher.activeSpace == space) {
				switcher.SwitchTo("SceneSpace");
			} else {
				switcher.SwitchTo(space);
			}
		}
	}
}
