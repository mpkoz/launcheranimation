﻿using UnityEngine;
using System.Collections;

public class Multispatial : MonoBehaviour {

	//TODO: Total refactor
	//NOTE: Should be placed on the object while it is in "Normal" space

	public GameObject HUDSpace;
	public GameObject PauldronSpace;
	public GameObject SceneSpace;

	public GameObject SceneSpaceSelf;
	public GameObject HUDSpaceSelf;
	public GameObject PauldronSpaceSelf;

	public string activeSpace = "SceneSpace";

	public float followSpeed = 2f;

	static GameObject ActiveHUDSpaceObject;
	static GameObject ActivePauldronSpaceObject;

	// Use this for initialization
	void Start () {
		//Find the spaces
		if (HUDSpace == null) {
			HUDSpace = GameObject.FindGameObjectWithTag ("HUDSpace");
		}

		if (PauldronSpace == null) {
			PauldronSpace = GameObject.FindGameObjectWithTag ("PauldronSpaceMarker");
		}

		if (SceneSpace == null) {
			SceneSpace = transform.parent.gameObject;
		}

		if (SceneSpaceSelf == null) {
			SceneSpaceSelf = new GameObject(gameObject.name + " multispatial marker");
			SceneSpaceSelf.transform.parent = SceneSpace.transform;
			SceneSpaceSelf.transform.localPosition = transform.localPosition;
			SceneSpaceSelf.transform.localRotation = transform.localRotation;
			SceneSpaceSelf.transform.localScale = transform.localScale;
		}

		//Need a place to exist in the HUDSpace & Pauldron. Look for some place that already exists (and is thus placed) - if it doesn't exist, create it
		if (HUDSpaceSelf == null && HUDSpace != null) {
			if(HUDSpace.transform.FindChild (gameObject.name)) {
				HUDSpaceSelf = HUDSpace.transform.FindChild (gameObject.name + " multispatial marker").gameObject;
			}

			HUDSpaceSelf = new GameObject(gameObject.name + " multispatial marker");
			HUDSpaceSelf.transform.parent = HUDSpace.transform;
			HUDSpaceSelf.transform.localPosition = new Vector3(0, 0, 0);
			HUDSpaceSelf.transform.localEulerAngles = new Vector3(0, 0, 0);
			HUDSpaceSelf.transform.localScale = new Vector3(1, 1, 1);
		}

		if (PauldronSpaceSelf == null && PauldronSpace != null) {
			if(PauldronSpace.transform.FindChild (gameObject.name + " multispatial marker")) {
				PauldronSpaceSelf = PauldronSpace.transform.FindChild (gameObject.name).gameObject;
			}

			PauldronSpaceSelf = new GameObject(gameObject.name + " multispatial marker");
			PauldronSpaceSelf.transform.parent = PauldronSpace.transform;
			PauldronSpaceSelf.transform.localPosition = new Vector3(0, 0, 0);
			PauldronSpaceSelf.transform.localEulerAngles = new Vector3(0, 0, 0);
			PauldronSpaceSelf.transform.localScale = new Vector3(1, 1, 1);
		}
	}
	
	// Update is called once per frame
	void Update () {
		Transform target = null;
		switch (activeSpace) {
		case "SceneSpace":
			if(SceneSpaceSelf != null) {
				target = SceneSpaceSelf.transform;
			}
			break;
		case "HUDSpace":
			if(HUDSpaceSelf != null) {
				target = HUDSpaceSelf.transform;
			}
			break;
		case "PauldronSpace":
			if(PauldronSpaceSelf != null) {
				target = PauldronSpaceSelf.transform;
			}
			break;
		default:
			return;
		}
		if (target == null) {
			return;
		}

		float fraction = Time.deltaTime * followSpeed;
		transform.localPosition = Vector3.Lerp (transform.localPosition, target.localPosition, fraction);
		transform.localRotation = Quaternion.Lerp (transform.localRotation, target.localRotation, fraction);
		transform.localScale = Vector3.Lerp (transform.localScale, target.localScale, fraction);
	}

	public void SwitchTo(string space) {
		switch (space) {
		case "SceneSpace":
			if(SceneSpace != null) {
				gameObject.transform.parent = SceneSpace.transform;
				activeSpace = space;

				if(gameObject == ActiveHUDSpaceObject) {
					ActiveHUDSpaceObject = null;
				}
				if(gameObject == ActivePauldronSpaceObject) {
					ChangeToLayer(gameObject, LayerMask.NameToLayer("Default"));
					ActivePauldronSpaceObject = null;
				}
			}
			break;
		case "HUDSpace":
			if(HUDSpace != null) {
				
				if(ActiveHUDSpaceObject != null) {
					ActiveHUDSpaceObject.GetComponent<Multispatial>().SwitchTo("SceneSpace");
				}

				gameObject.transform.parent = HUDSpace.transform;
				ActiveHUDSpaceObject = gameObject;
				activeSpace = space;

			}
			break;
		case "PauldronSpace":
			if(ActivePauldronSpaceObject != null) {
				ActivePauldronSpaceObject.GetComponent<Multispatial>().SwitchTo("SceneSpace");
			}

			if(PauldronSpace != null) {

				Transform ARRoot = Camera.main.transform.root;
				Vector3 relativePos = ARRoot.InverseTransformVector(PauldronSpace.transform.root.position);

				transform.SetParent(PauldronSpace.transform);
				transform.localPosition = relativePos;

				ChangeToLayer(gameObject, LayerMask.NameToLayer("PauldronLayer"));
				ActivePauldronSpaceObject = gameObject;
				activeSpace = space;
			}
			break;
		}
	}

	public static void ChangeToLayer(GameObject go, int l) {
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
			trans.gameObject.layer = l;
		}
	}
}