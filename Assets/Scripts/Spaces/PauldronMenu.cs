﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

//TODO: Comprehend, then write more TODO's

public class PauldronMenu : MonoBehaviour {

	public float speed = 0.1f;
	//public float snapDistance = 1;
    private float pauldronEdge = 20f;

    public float pauldronZ = 40;

    public float centerRange;
    
    private float delay;

    GameObject center;

    public float distanceFromObject = 6;
    
    //private float pauldronEdgeDistance

    Quaternion startRotation;
    bool moved = false;
    float startTime;
    bool timerStarted = false;
    bool snapToCamera = false;


	void Awake()
	{
        Vector3 pauldronMidPoint = new Vector3(Screen.width/2,
                                               Screen.height/2,
                                               pauldronZ);

        
        center = new GameObject("Pauldron Center");
        Vector3 pauldronCenter = Camera.main.ScreenToWorldPoint(pauldronMidPoint);
        center.transform.position = pauldronCenter;
        center.transform.parent = Camera.main.transform;
	}
    
    
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        //pauldronFairy();
        Pauldron();

   	}


    void Pauldron()
    {
        int num_children =  transform.childCount;

        bool lock_to_screen = true;
        
        for (int i = 0 ; i < num_children; i++)
        {
            if (Vector3.Distance(transform.GetChild(i).position, center.transform.position) < distanceFromObject)
            {
                lock_to_screen = false;
            }
        }
        if (lock_to_screen)
        {
            smoothToCamera();            
        }
   
    }
    
    
	private void smoothToCamera() {

        Quaternion cameraQuaternions = Camera.main.transform.rotation;

        if (!moved)
        {
            startRotation = cameraQuaternions;
            moved = true;
        }

        if (!timerStarted)
        {
            if (Quaternion.Angle(startRotation, cameraQuaternions) > centerRange)
            {
                startTime = Time.time;
                timerStarted = true;
            }
        }

        float currentTime = Time.time;

        if (timerStarted && snapToCamera == false)
        {
            if ((currentTime - startTime) > 1)
            {
                snapToCamera = true;
                timerStarted = false;
            }
        }

        //Snap To Camera
        if (snapToCamera)
        {
            Quaternion currentTarget = Quaternion.Slerp(transform.rotation,
                                                        cameraQuaternions,
                                                        Time.deltaTime * speed);
            transform.rotation = currentTarget;
            if (Quaternion.Angle(transform.localRotation, cameraQuaternions) < centerRange)
            {
                snapToCamera = false;
                moved = false;
            }
        }
        
		// Match them and try to bounce back
    }

    
    
    
    private void pauldronFairy()
    {
        Quaternion cameraQuaternions = Camera.main.transform.rotation;
        
        //MeasureRotationAngle();

		// Match them and try to bounce back
        
        if (Quaternion.Angle(transform.rotation, Camera.main.transform.rotation) > pauldronEdge)
            {
                Quaternion currentTarget = Quaternion.Slerp (transform.rotation,
                                                            cameraQuaternions,
                                                            Time.deltaTime * speed);
                transform.rotation = currentTarget;
            }
    }


    
    // 	public void updateBasedOnImu() {
    // 		Vector3 targetRotation = UsbImuController.getInstance().getEulerAngle();

    // 		float currentDistance = Vector3.Distance (targetRotation, currentRelativeRotation);
    // 		Debug.Log ("Pauldron: currentDistance "+currentDistance.ToString());
    // 		if (currentDistance < snapDistance) {
    // 			currentRelativeRotation = targetRotation;
    // //			transform.rotation = Camera.main.transform.rotation;
    // 			smoothToCamera();
    // 		} else {
    // 			Debug.Log ("Pauldron: following IMU");
    // 			// Move towards
    // 			// We take the difference between the camera rotation and the targetRotation and apply it afterwards

    // 			Vector3 updatedRotation = Vector3.Lerp (currentRelativeRotation, targetRotation, Time.deltaTime * speed);
    // 			Vector3 deltaRotation = updatedRotation - currentRelativeRotation;
    // 			// Apply the deltaRotataion to the currentRelativeRotation to keep it updated
    // 			currentRelativeRotation = updatedRotation;
    // 			Debug.Log ("Pauldron: delta "+deltaRotation.ToString());
    // 			// Apply the -delta location to the real rotation to move the object
    // 			transform.Rotate (deltaRotation);
    // 		}
    //		// We locate ourselfs on the camera
    //		transform.position = Camera.main.transform.position; 
    //		// We rotate towards the target rotation
    //		transform.Translate(new Vector3(0,0,-10), Camera.main.transform.up8);
    //		// NOTE: Target rotation depends on which shoulder (+-30 degrees)
    //   }






    
    
}
