﻿using UnityEngine;
using System.Collections;

public class ContentPosition : MonoBehaviour {

	//Set this to the transform you want to check
	public Transform objectTransform;

	private float noMovementThreshold = .004f;
	private const int noMovementFrames = 3;
	Vector3[] previousLocations = new Vector3[noMovementFrames];
	private bool isMoving;

	//Let other scripts see if the object is moving
	public bool IsMoving
	{
		get{ return isMoving; }
	}

	void Awake()
	{
		//objectTransform = transform.position;
		//For good measure, set the previous locations
		for(int i = 0; i < previousLocations.Length; i++)
		{
			previousLocations[i] = Vector3.zero;
		}
	}

	void Update()
	{
		//Store the newest vector at the end of the list of vectors
		for(int i = 0; i < previousLocations.Length - 1; i++)
		{
			previousLocations[i] = previousLocations[i+1];
		}
		previousLocations[previousLocations.Length - 1] = objectTransform.position;

		//Check the distances between the points in your previous locations
		//If for the past several updates, there are no movements smaller than the threshold,
		//object isnt moving
		for(int i = 0; i < previousLocations.Length - 1; i++)
		{
			if(Vector3.Distance(previousLocations[i], previousLocations[i + 1]) >= noMovementThreshold)
			{
				isMoving = true;
			}
			else
			{
				isMoving = false;
				break;
			}
		}

		//print ("isMoving: " + isMoving);
		//print("content position: " + this.transform.position);

	}

}
