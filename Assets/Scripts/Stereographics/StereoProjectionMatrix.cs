﻿using UnityEngine;
using System.Collections;

public class StereoProjectionMatrix : MonoBehaviour {

	// Use this for initialization
	void Start () {


		//
		// The phyiscal position of the lumis lens and human eyeball were  used to calculate the virtual cameras projection matrix 
		// for Stereoscopic rendering where the view-frustrum is aligned with the active area of the lumis display
		// this is "skewed-frustrum stereoscopics" 
		// 
		// see here for measurements and more details
		//https://docs.google.com/spreadsheets/d/1v9L9HdmrMefEJXhEWKaNlImKG2B_Opn6DHlKJ9eDZSI/

		Matrix4x4 p = new Matrix4x4(); 
		p[0, 0] = 2.28070f;
		p[0, 2] = 0.0f;
		p[1, 1] = 4.06250f;
		p[1, 2] = 0.0f;
		p[2, 2] = -1.0f;
		p[2, 3] = -0.03900f;
		p[3, 2] = -1.0f;
		this.GetComponent<Camera>().projectionMatrix = p;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
