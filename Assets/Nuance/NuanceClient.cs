﻿using UnityEngine;
using System.Collections;
using Mono.Unix;
using System.Net.Sockets;

public class NuanceClient : MonoBehaviour 
{
	private UnixClient client;
	private NetworkStream stream;

	void DebugPrintJson (string jsonString)
	{
		Debug.Log (jsonString);

		NuanceJson json = new NuanceJson ();
		json = JsonUtility.FromJson<NuanceJson> (jsonString);

		print ("Print statement");

		Debug.Log ("_resultType: " + json._resultType);
		Debug.Log ("_isSpeech: " + json._isSpeech);
		Debug.Log ("_isInGrammar: " + json._isInGrammar);
		Debug.Log ("_hypotheses: ");

		foreach (NuanceJson.Hypothesis hypothesis in json._hypotheses) 
		{
			Debug.Log ("\t{");
			Debug.Log ("\t_startRule: " + hypothesis._startRule);
			Debug.Log ("\t_conf: " + hypothesis._conf);
			Debug.Log ("\t_score: " + hypothesis._score);
			Debug.Log ("\t_beginTimeMs: " + hypothesis._beginTimeMs);
			Debug.Log ("\t_endTimeMs: " + hypothesis._endTimeMs);
			Debug.Log ("\t_lmScore: " + hypothesis._lmScore);
			Debug.Log ("\t_items: ");

			foreach (NuanceJson.Item item in hypothesis._items) 
			{
				Debug.Log ("\t\t{");
				Debug.Log ("\t\t_type: " + item._type);
				Debug.Log ("\t\t_orthography: " + item._orthography);
				Debug.Log ("\t\t_conf: " + item._conf);
				Debug.Log ("\t\t_score: " + item._score);
				Debug.Log ("\t\t_beginTimeMs: " + item._beginTimeMs);
				Debug.Log ("\t\t_endTimeMs: " + item._endTimeMs);
				Debug.Log ("\t\t}");
			}
			Debug.Log ("\t}");
		}
	}

	void Start () 
	{
		client = new UnixClient ("/home/andrew/echo_socket");
		stream = client.GetStream ();
	}

	public void GetJson ()
	{
		Debug.Log ("Get JSON");
		string str = "test";
		byte[] writeBytes = System.Text.Encoding.ASCII.GetBytes (str);
		stream.Write (writeBytes, 0, writeBytes.Length);

		byte[] readBytes = new byte[10000];
		stream.Read (readBytes, 0, readBytes.Length);

		string jsonString = System.Text.Encoding.ASCII.GetString (readBytes);

		Debug.Log (jsonString);
		//Debug.Log (System.Text.Encoding.ASCII.GetString(readBytes));

		//DebugPrintJson (jsonString);

		//System.Diagnostics.Process.Start ("/usr/bin/gedit");
	}


}


