﻿using System;

[Serializable]
public class NuanceJson
{
	[System.Serializable]
	public class Item
	{
		public string _type;
		public string _orthography;
		public int _conf;
		public int _score;
		public int _beginTimeMs;
		public int _endTimeMs;
	}

	[System.Serializable]
	public class Hypothesis
	{
		public string _startRule;
		public int _conf;
		public int _score;
		public int _beginTimeMs;
		public int _endTimeMs;
		public int _lmScore;
		public Item[] _items;
	}

	public string _resultType;
	public string _isSpeech;
	public string _isInGrammar;
	public Hypothesis[] _hypotheses;
}
