﻿using UnityEngine;
using System.Collections;

public class MenuCanvasColliders : MonoBehaviour {

	public GameObject reticleFollower;
	public GameObject buttonWatcher;
	public GameObject launcherButton;
	public Animator scrollAnimator;
	public Animation scrollUp;

	private GameObject[] quads;
	private bool hasBeenRan;

	// Use this for initialization
	void Start () {
		hasBeenRan = false;

	}
	
	// Update is called once per frame
	void Update () {
		//quads = GameObject.FindGameObjectsWithTag ("Quads");
		if (reticleFollower.GetComponent<ReticleFollower> ().getItem () == this.transform && reticleFollower.GetComponent<ReticleFollower>().getIsInItem() == true) {


			Minimize();
			hasBeenRan = true;



//			scrollAnimator.Play("ScrollUp", -1, 1f);
//			scrollAnimator.ResetTrigger ("scrollDown");
//			buttonWatcher.GetComponent<ButtonWatcher> ().setScrolledUp (true);

				



		}
	}

	public IEnumerator ShowButton(){
		yield return new WaitForSeconds(2);
		launcherButton.SetActive (true);

		if (buttonWatcher.GetComponent<ButtonWatcher> ().getScrolledUp() == false) {
			scrollAnimator.speed = 2.0f;
			scrollAnimator.SetTrigger ("scrollUp");
			scrollAnimator.ResetTrigger ("scrollDown");
			buttonWatcher.GetComponent<ButtonWatcher> ().setScrolledUp (true);
		}
	}

	public void Minimize(){
		buttonWatcher.GetComponent<ButtonWatcher> ().setIsMinimized (true);
		StartCoroutine (ShowButton ());
	}
}
