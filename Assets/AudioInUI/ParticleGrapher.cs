﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (ParticleSystem))]

public class ParticleGrapher : MonoBehaviour {

	private int resolution = 100;

	private int currentResolution;
	public ParticleSystem.Particle[] points;

    private ParticleSystem particleSystem;
    private Particle model;

    
    void Start() {
                
        particleSystem = GetComponent<ParticleSystem>();

        //Change Foreground to the layer you want it to display on 
        //You could prob. make a public variable for this
        particleSystem.GetComponent<Renderer>().sortingLayerName = "Water";

    }

    void Update(){

    }

   
    
    public void nudgeParticles(Vector3[] amount) {


        for (int i = 0; i <points.Length; i++) {
            points[i].position += amount[i];
        }
        particleSystem.SetParticles(points, points.Length);
    }



    void instantiateParticles() {
        if (points == null) {
            points = new ParticleSystem.Particle[resolution];
        }
    }

    public void ShapeParticles() {
        instantiateParticles();

        
    }
    
    public void MakeCircle(float radius) {
        instantiateParticles();

        int[] circle = SplitNegate(resolution);
            
        for (int i = 0; i < points.Length; i++ ){
            float z = SemiCircle( circle[i], radius );
            points[i].position = new Vector3(i*0.01f, 0, 0);
			points[i].startSize = 0.1f;
            points[i].color = Color.red;
        }
        particleSystem.SetParticles(points, points.Length);
    }


    public void ChangePoints(Vector3[] positions, Color color, float size ) {
        
          for (int i = 0; i < positions.Length; i++ ) {
            points[i].position = positions[i];
            points[i].size = size;
            points[i].color = color;
        }
    }
   

    private void CreateLine()
    {
        points = new ParticleSystem.Particle[resolution];
        for (int i = 0; i < resolution; i++)
        {
            float x = i;
            points[i].position = new Vector3(x, 0f, 0f);
            points[i].color = new Color(x, 0f, 0f);
            points[i].size = 0.00001f;
			points [i].startSize = 0.000001f;
        }
    }

    private int[] SplitNegate(int size)
    {
        int[] forCircle  = new int[size];

        int number = -(size/2);
        
        for ( int i = 0; i < size; i++)
        {
            forCircle[i] = number;
            number++;
        }
        return forCircle;
    }






    private static float Exponential (float x) {
        return x * x;
	}

	private static float Parabola (float x){
		x = 2f * x - 1f;
		return x * x;
	}

	private static float Sine (float x, int split){
        float d = split;
		return  Mathf.Sin(2 * x/d)*d;
	}

    private float SemiCircle(float x, float r)
    {
        return Mathf.Sqrt(Mathf.Pow(r,2) - Mathf.Pow(x,2));
    }
    
}
