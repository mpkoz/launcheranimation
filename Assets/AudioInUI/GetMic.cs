﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class GetMic : MonoBehaviour {

    private int minFreq;
    private int maxFreq;


    private AudioSource goAudioSource;

    // public float sensitivity = 100;
    // public float loudness = 0;
    
	// Use this for initialization
	void Start () {

        
        goAudioSource = GetComponent<AudioSource>();
        
        goAudioSource.clip = Microphone.Start(null, true, 1, 6000);

        goAudioSource.loop = true;
        goAudioSource.mute = false;
        while (!(Microphone.GetPosition(null) > 0)) {
            
        }
        goAudioSource.Play();

    }

    void Update() {
    }



    
}
