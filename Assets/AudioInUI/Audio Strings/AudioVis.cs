﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (ParticleGrapher))]

[RequireComponent (typeof (AudioSource))]

public class AudioVis : MonoBehaviour {

    float[] data;

    public AudioSource source;

    int sampleSize = 2048;

    ParticleGrapher pg;

    
    void Awake()
    {
        pg = GetComponent<ParticleGrapher>();
        
        data = new float[sampleSize];
        
    }



    void Update()
    {
        
        cirlceParticleWave();
    }
   
    
    void ParticleWave()
    {
        source.GetOutputData(data, 0);
        
        Vector3[] samplePositions = new Vector3[data.Length];
        
        for (int i = 0; i < data.Length; i++)
        {
			samplePositions[i] = new Vector3(0, data[i]*10, 0);
        }
        
        pg.nudgeParticles(samplePositions);
    }

    void cirlceParticleWave() {
        pg.MakeCircle(250);
        ParticleWave();
    }
    
    void circleParticleMonster() {

        if (pg.points == null)
            pg.MakeCircle(250);

        ParticleWave();
     }
}
