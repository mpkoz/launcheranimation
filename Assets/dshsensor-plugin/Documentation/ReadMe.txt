The 4DOS Unity plugin allows the user to access the different sensors on the Thor Platform.
Please check the release notes for which sensors are currently supported.

There are 2 scenes present with this drop -
1. DSHCamera.unity -
This showcases access to the Camera/Tracking. There is a GameObject DSHCamera which acts as the main controller which allows user to enable/disable video background rendering, enable/disable stereo rendering, enable/disable tracking. The user needs to do the following steps -
	a. Create a DSHCamera GameObject and attach the DSHCamera script to it.
	b. Make sure to set appropriate values for the Camera Params (camera_para_realsense_tusi_640x480-updated works well for the Thor helmets), Near Plane (0.01), Far Plane (1000)
	c. Create an Trackable GameObject from the Prefab provided (ARTrackable) inside Prefabs folder.
	d. Set the appropriate target .png file. composite_2048_png.png is included with the project as an example.
	e. Make sure to link this Trackable game object to the DSHCamera  game object which was created in step a.

2. DSHIMU.unity -
This showcases access to the IMU. To get access, attach the DSHIMUSensor script to the object as shown in the scene, that will get access to the IMU data in the Update function.

IMPORTANT: Please add ARForeground and Reticle Layers by going to Edit->Project Settings->Tags and Layers menu item. Currently this is not being exported as part of the unitypackage.

Please contact apar.suri@daqri.com or naveen.gunalan@daqri.com regarding questions.
Note: The camera/imu services should be running while trying to run the Unity applications. Please contact the developers if you do not understand what that means.
