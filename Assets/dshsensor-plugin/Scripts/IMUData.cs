using System;
using UnityEngine;

//TODO add namespace
public class IMUData
{
	private const int IMU_DATA_SIZE=18;

	//private float[] imuData = new float[IMU_DATA_SIZE];
	private Vector3 gyro = new Vector3 (float.NaN, float.NaN, float.NaN);
	private Vector3 acc = new Vector3 (float.NaN, float.NaN, float.NaN);
	private Vector3 mag = new Vector3 (float.NaN, float.NaN, float.NaN);
	private Vector3 eul = new Vector3 (float.NaN, float.NaN, float.NaN);
	private Quaternion quat = new Quaternion (float.NaN, float.NaN, float.NaN, float.NaN);

	public Vector3 Gyro {
		get {
			return gyro;
		}
	}
	public Vector3 Acc {
		get {
			return acc;
		}
	}
	public Vector3 Mag {
		get {
			return mag;
		}
	}
	public Vector3 Eul {
		get {
			return eul;
		}
	}
	public Quaternion Quat {
		get {
			return quat;
		}
	}
	public IMUData ()
	{

	}

	public void SetNativeData(float[] data) {
		if (data.Length < IMU_DATA_SIZE) {
			Debug.LogWarning("IMU size incorrect -- " + data.Length);
			return;
		}
		int index = 0;
		int dim = 3;
		for (int i = 0; i < dim; ++i) {
			gyro[i] = data[index++]; 
		}
		
		for (int i = 0; i < dim; ++i) {
			acc[i] = data[index++]; 
		}

		for (int i = 0; i < dim; ++i) {
			mag[i] = data[index++]; 
		}

		for (int i = 0; i < dim; ++i) {
			eul[i] = data[index++]; 
		}
		dim = 4;
		for (int i = 0; i < dim; ++i) {
			quat[i] = data[index++]; 
		}
	}
}




