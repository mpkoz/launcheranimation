using UnityEngine;
using System.Collections;

public interface TrackingEventListener {
	void OnTrackingFound(ARTrackable obj);
	void OnTrackingLost(ARTrackable obj);
}

public class ARTrackable : MonoBehaviour {	 

	bool visible;
	Matrix4x4 transformationMatrix;
	bool inited = false;
	Matrix4x4 projectionMatrix;
	private bool stereoRendering = false;
	bool prevVisible;

	[SerializeField]
	private static ArrayList trackingEventsListeners;

	[HideInInspector]
	public static void AddTrackingEventListener(TrackingEventListener listener) {
		if (trackingEventsListeners == null) {
			trackingEventsListeners = new ArrayList();
		}
		Debug.Log ("Adding event listener");
		trackingEventsListeners.Add(listener);
	}

	[HideInInspector]
	public static void RemoveTrackingEventListener(TrackingEventListener listener) {
		if (trackingEventsListeners == null) {
			trackingEventsListeners = new ArrayList();
			Debug.LogWarning("Listener list empty, not present in list of TrackingEventListeners");
			return;
		}
		trackingEventsListeners.Remove (listener);
	}

	[HideInInspector]
	public static void ResetTrackingEventListener() {
		if (trackingEventsListeners == null) {
			trackingEventsListeners = new ArrayList();
		}
		trackingEventsListeners.Clear ();
	}
	
	//private Camera[] arCameras;
	private GameObject goARCameras;
	private ArrayList arCameras;
	private Matrix4x4[] opticalSeeThroughMatrix = new Matrix4x4[2];
	int arCameraCount = 0;
	private bool enableTracking = true;
	[SerializeField][HideInInspector]
	private bool enableVideoRendering = true;
	[HideInInspector]
	public bool EnableVideoRendering
	{
		get 
		{
			return enableVideoRendering;
		}
		set
		{
			if (enableVideoRendering != value)
			{
				enableVideoRendering = value;
				ToggleVideoForeground();
			}
		}
	}

	[HideInInspector]
	public float NearPlane = 0.01f;
	[HideInInspector]
	public float FarPlane = 1000.0f;//5.0f;
	public string ARTVisionImageName = "";

	[HideInInspector]
	public bool StereoRendering 
	{
		get 
		{
			return stereoRendering;
		}
		set
		{
			if (stereoRendering != value) {
				stereoRendering = value;
				Init();
			}
		}
	}

	[HideInInspector]
	public bool EnableTracking 
	{
		get 
		{
			return enableTracking;
		}
		set
		{
			if (enableTracking != value) {
				enableTracking = value;
				Init();

				{
					ToggleTracking(enableTracking);
				}
			}
		}
	}


	void ToggleTracking(bool enable)
	{
		if (arCameras != null) {
			foreach (Camera c in arCameras)
			{
				c.gameObject.SetActive(enable);
			}
		}

		if (goARExperience != null) {
			Debug.Log("Toggling ARExperience, setting to " + enable);
			goARExperience.SetActive(enable);
		} else {
			Debug.LogWarning("ARExperience is not found in the Trackable child-tree");
		}
	}

	void ToggleVideoForeground()
	{
		if (arCameras != null) {
			foreach (Camera c in arCameras)
			{
				if (enableVideoRendering) {
					c.clearFlags = CameraClearFlags.Depth;
				}
				else {
					c.backgroundColor = Color.black;
					c.clearFlags = CameraClearFlags.SolidColor;
				}
			}
		}
	}

	//ARExperience gameobject
	private GameObject goARExperience;

	void Init() {
		inited = false;
			
		if (goARExperience == null) {
			//goARExperience = GameObject.Find ("ARExperience");
			//parse through the children and find ARExperience
			foreach (Transform child in transform) 
			{
				if (child.name.Contains ("ARExperience")) {
					Debug.Log("Found ARExperience");
					goARExperience = child.gameObject;
					goARExperience.transform.localScale = new Vector3(1, -1, 1);
				}
				if (child.name.Contains ("ARCameras")) {
					Debug.Log("Found ARCameras");
					goARCameras = child.gameObject;
				}
			}
			if (goARExperience == null) {
				Debug.LogError ("ARExperience gameobject missing. Please add ARExperience gameobject which parents the AR content");
			}
		}
		
		arCameraCount = 0;
		if (arCameras == null) {
			arCameras = new ArrayList ();
		} else {
			arCameras.Clear ();
		}

		//
		foreach (Transform child in goARCameras.transform) {
			if (child.name.Contains ("ARCamera") && child.GetComponent<Camera> () != null) {
				if (StereoRendering != true && arCameraCount >= 1) {
					//Debug.Log("child name -- " + child.name);
					child.gameObject.SetActive (false);
					arCameraCount++;
					//break;
					continue;
				}
				if (arCameraCount >= 2) {
					arCameraCount++;
					//break;
					continue;
				}
				
				arCameras.Add (child.GetComponent<Camera> ());
				child.gameObject.SetActive (true);
				arCameraCount++;
				
				if (child.name == "ARCamera") {
					Camera c = child.GetComponent<Camera> ();
					
					if (c != null) {
						if (stereoRendering != true) {
							c.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
						} else {
							c.rect = new Rect (0.0f, 0.0f, 0.5f, 1.0f);
						}
					}
				}
			}
		}
		//-----------------------------------------------------------
		/*foreach (Transform child in transform) {
			if (child.name.Contains ("ARCamera") && child.GetComponent<Camera> () != null) {
				//Debug.Log("Child : " + child.name);
				if (StereoRendering != true && arCameraCount >= 1) {
					//Debug.Log("child name -- " + child.name);
					child.gameObject.SetActive(false);
					arCameraCount++;
					//break;
					continue;
				}
				if (arCameraCount >= 2) {
					arCameraCount++;
					//break;
					continue;
				}
				
				arCameras.Add (child.GetComponent<Camera> ());
				child.gameObject.SetActive(true);
				arCameraCount++;
				
				if (child.name == "ARCamera")
				{
					Camera c = child.GetComponent<Camera>();
					
					if (c != null)
					{
						if (stereoRendering != true)
						{
							c.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
						}
						else 
						{
							c.rect = new Rect (0.0f, 0.0f, 0.5f, 1.0f);
						}
					}
				}
			}
		}*/
	}

	void Awake(){
		Init ();
	}

	// Use this for initialization
	void Start () {

	}

	public bool SetupCamera(Camera ar, float nearClipPlane, float farClipPlane, int index)//, Matrix4x4 projectionMatrix, ref bool opticalOut)
	{
		//Naveen
		//originalProjectionMatrix = projectionMatrix;
		
		Camera c = ar;
		
		// A perspective projection matrix from the tracker
		c.orthographic = false;
		
		// Shouldn't really need to set these, because they are part of the custom 
		// projection matrix, but it seems that in the editor, the preview camera view 
		// isn't using the custom projection matrix.
		c.nearClipPlane = nearClipPlane;
		c.farClipPlane = farClipPlane;

		ARCamera arc = c.gameObject.GetComponent<ARCamera> ();

		if (arc != null) 
		{
			if (arc.OpticalSeeThrough)
			{
				float fovy = 0.0f;
				float aspect = 0.0f;
				float[] m = new float[16];
				float[] p = new float[16];
				byte[] oparam0 = null;
				bool opticalSetup = false;

				if (!string.IsNullOrEmpty(arc.OpticalParamFile)) {
					TextAsset ta;								
					ta = Resources.Load("ardata/optical/" + arc.OpticalParamFile, typeof(TextAsset)) as TextAsset;
				
					if (ta == null) {		
						// Error - the camera_para.dat file isn't in the right place			
						Debug.Log("StartAR(): Error: Camera parameters file not found at Resources/ardata/optical" + arc.OpticalParamFile + ".bytes");
						//return;// (false);
					}
					else {
						oparam0 = ta.bytes;
						Debug.Log("asset loaded successfully");
						opticalSetup = true;
					}				
				}

				if (opticalSetup) {
					Debug.Log("calling GetOpticalParameters");
					opticalSetup = VisionUnityPlugin.GetOpticalParameters(oparam0, oparam0.Length, out fovy, out aspect, m, p);
					Debug.Log("returned from GetOpticalParameters -- " + opticalSetup);
				}

				if (!opticalSetup) 
				{
					//Debug.Log("optical setup failed");
					//vbc.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), Vector3.one) * vbc.projectionMatrix;

					c.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), Vector3.one) * projectionMatrix;
				}
				else 
				{
					m[12] *= 0.001f;
					m[13] *= 0.001f;
					m[14] *= 0.001f;
					Debug.Log("Optical parameters: fovy=" + fovy  + ", aspect=" + aspect + ", camera position (m)={" + m[12].ToString("F3") + ", " + m[13].ToString("F3") + ", " + m[14].ToString("F3") + "}");
					
//						StringBuilder sb = new StringBuilder();
//						foreach(float f in m)
//						{
//							sb.Append(f + ", ");
//						}
					//Debug.Log("view matrix m " + sb.ToString());
					//c.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), Vector3.one) * ARUtilityFunctions.MatrixFromFloatArray(p);
					c.projectionMatrix = Matrix4x4.Scale(new Vector3(1, -1, 1)) * ARUtilityFunctions.MatrixFromFloatArray(p);

					//opticalseethroughProjectionMatrix = ARUtilityFunctions.MatrixFromFloatArray(p);;
					
					Matrix4x4 opticalViewMatrix = ARUtilityFunctions.MatrixFromFloatArray(m);
					Debug.Log("optical mat -- " + opticalViewMatrix);
					//if (OpticalEyeLateralOffsetRight != 0.0f) opticalViewMatrix = Matrix4x4.TRS(new Vector3(-OpticalEyeLateralOffsetRight, 0.0f, 0.0f), Quaternion.identity, Vector3.one) * opticalViewMatrix; 
					opticalViewMatrix = Matrix4x4.TRS(arc.PositionOffset, Quaternion.Euler(arc.RotationOffset), Vector3.one) * opticalViewMatrix;
					//Debug.Log("optical mat after -- " + opticalViewMatrix);
					// Convert to left-hand matrix.
					//opticalViewMatrixOriginal = opticalViewMatrix;
					//opticalViewMatrix = ARUtilityFunctions.LHMatrixFromRHMatrix(opticalViewMatrix);			

					if (index >= 1)
					{
						opticalSeeThroughMatrix[1] = opticalViewMatrix;
					}
					else {
					opticalSeeThroughMatrix[0] = opticalViewMatrix;
					}
				}

				}
				else
				{
					//c.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), Vector3.one) * projectionMatrix;
					c.projectionMatrix = Matrix4x4.Scale(new Vector3(1, -1, 1)) * projectionMatrix;
				}
			}
			else
			{
				//c.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), Vector3.one) * projectionMatrix;
				c.projectionMatrix = Matrix4x4.Scale(new Vector3(1, -1, 1)) * projectionMatrix;
			}
		
//		if (Optical) {
//			float fovy ;
//			float aspect;
//			float[] m = new float[16];
//			float[] p = new float[16];
//			opticalSetupOK = PluginFunctions.arwLoadOpticalParams(null, OpticalParamsFileContents, OpticalParamsFileContents.Length, out fovy, out aspect, m, p);
//			if (!opticalSetupOK) {
//				ARController.Log(LogTag + "Error loading optical parameters.");
//				return false;
//			}
//			m[12] *= 0.001f;
//			m[13] *= 0.001f;
//			m[14] *= 0.001f;
//			ARController.Log(LogTag + "Optical parameters: fovy=" + fovy  + ", aspect=" + aspect + ", camera position (m)={" + m[12].ToString("F3") + ", " + m[13].ToString("F3") + ", " + m[14].ToString("F3") + "}");
//			
//			StringBuilder sb = new StringBuilder();
//			foreach(float f in m)
//			{
//				sb.Append(f + ", ");
//			}
//			//Debug.Log("view matrix m " + sb.ToString());
//			c.projectionMatrix = ARUtilityFunctions.MatrixFromFloatArray(p);
//			
//			//Naveen
//			opticalseethroughProjectionMatrix = ARUtilityFunctions.MatrixFromFloatArray(p);;
//			
//			opticalViewMatrix = ARUtilityFunctions.MatrixFromFloatArray(m);
//			if (OpticalEyeLateralOffsetRight != 0.0f) opticalViewMatrix = Matrix4x4.TRS(new Vector3(-OpticalEyeLateralOffsetRight, 0.0f, 0.0f), Quaternion.identity, Vector3.one) * opticalViewMatrix; 
//			// Convert to left-hand matrix.
//			opticalViewMatrixOriginal = opticalViewMatrix;
//			opticalViewMatrix = ARUtilityFunctions.LHMatrixFromRHMatrix(opticalViewMatrix);
//			
//			opticalOut = true;
//		} 

		
		// Don't clear anything or else we interfere with other foreground cameras
		//c.backgroundColor = Color.clear;\

		c.backgroundColor = Color.black;
		c.clearFlags = CameraClearFlags.SolidColor;
		
		// Renders after the clear and background cameras
		c.depth = 2;
		
		//c.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		//c.transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
		//c.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

		if (!stereoRendering) 
		{
			c.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
		}
		ToggleVideoForeground ();
		return true;
	}


	// Update is called once per frame
	void Update () {

		if (enableTracking) {

			if (goARExperience == null) {
				goARExperience = GameObject.Find ("ARExperience");

				if (goARExperience == null)
					return;
			}

			if (!inited) {
				float[] projMatrixRawArray = new float[16];

				VisionUnityPlugin.GetProjectionMatrix (projMatrixRawArray);
				Matrix4x4 projMatrixRaw = ARUtilityFunctions.MatrixFromFloatArray (projMatrixRawArray);
				projectionMatrix = projMatrixRaw;//ARUtilityFunctions.LHMatrixFromRHMatrix(projMatrixRaw);
				//projectionMatrix = projectionMatrix * Matrix4x4.Scale( new Vector3(-1, 0, 0));
				Debug.Log ("Projection Mat: " + projectionMatrix);

				if (arCameras != null && arCameras.Count > 0) {
					int index = 0;
					foreach (Camera arCamera in arCameras) {
						SetupCamera (arCamera, NearPlane, FarPlane, index++);
					}
				}
				inited = true;
				prevVisible = visible;
			}
			float[] matrixRawArray = new float[16];
		
			// Query visibility if we are running in the Player.
			if (Application.isPlaying) {
			
				visible = VisionUnityPlugin.GetTransformation (matrixRawArray);
				//Debug.Log("ARTrackable.Update() -- visible=" + visible);
				bool shouldUpdateTrackingEvents = false;
				if (prevVisible != visible) {
					shouldUpdateTrackingEvents = true;
					prevVisible = visible;
				}
			
				if (visible) {
					if (shouldUpdateTrackingEvents && trackingEventsListeners != null) {
						foreach(TrackingEventListener listener in trackingEventsListeners)
						{
							if (listener != null) {
								listener.OnTrackingFound(this);
							}
							else {
								Debug.LogWarning("OnTrackingFound, but listener is null");
							}
						}
					}
//					if (!enableVideoRendering) {
//						matrixRawArray [12] *= -1.0f;
//					}
				
//					matrixRawArray [12] *= 0.001f; // Scale the position from ARToolKit units (mm) into Unity units (m).
//					matrixRawArray [13] *= 0.001f;
//					matrixRawArray [14] *= 0.001f;
					//matrixRawArray [13] = -matrixRawArray [13];

				
					Matrix4x4 matrixRaw = ARUtilityFunctions.MatrixFromFloatArray (matrixRawArray);
					//ARController.Log("arwQueryMarkerTransformation(" + UID + ") got matrix: [" + Environment.NewLine + matrixRaw.ToString("F3").Trim() + "]");
				
					// ARToolKit uses right-hand coordinate system where the marker lies in x-y plane with right in direction of +x,
					// up in direction of +y, and forward (towards viewer) in direction of +z.
					// Need to convert to Unity's left-hand coordinate system where marker lies in x-y plane with right in direction of +x,
					// up in direction of +y, and forward (towards viewer) in direction of -z.
					transformationMatrix = matrixRaw;//ARUtilityFunctions.LHMatrixFromRHMatrix(matrixRaw);

					if (arCameras != null && arCameras.Count > 0) 
					{
						if (visible) 
						{
							if (!goARExperience.activeInHierarchy)
								goARExperience.SetActive (true);
						
							Matrix4x4 pose;
//						if (Optical && opticalSetupOK) {
//							//pose = (opticalViewMatrix * marker.TransformationMatrix).inverse;
//							//					StringBuilder sb = new StringBuilder();
//							//					for (int i = 0; i < 16; ++i)
//							//					{
//							//						sb.Append(opticalViewMatrix[i] + ", ");
//							//					}
//							//Debug.Log("OpticalViewMatrix -- " + opticalViewMatrix.ToString());
//							pose = (opticalViewMatrix * marker.TransformationMatrix).inverse;
//						} 
//						else 

							pose = transformationMatrix.inverse;

						
							//arPosition = ARUtilityFunctions.PositionFromMatrix(pose);
							// Camera orientation: In ARToolKit, zero rotation of the camera corresponds to looking vertically down on a marker
							// lying flat on the ground. In Unity however, if we still treat markers as being flat on the ground, we clash with Unity's
							// camera "rotation", because an unrotated Unity camera is looking horizontally.
							// So we choose to treat an unrotated marker as standing vertically, and apply a transform to the scene to
							// to get it to lie flat on the ground.
							//arRotation = ARUtilityFunctions.QuaternionFromMatrix(pose);


							int index = 0;
							foreach (Camera arCamera in arCameras) 
							{
								ARCamera arc = arCamera.gameObject.GetComponent<ARCamera>();
								Matrix4x4 localPose = pose;
								if (arc != null) 
								{
									if (arc.OpticalSeeThrough && index < 2) 
									{
										localPose = opticalSeeThroughMatrix[index] * pose;
									}
								}
								Vector3 poseData = ARUtilityFunctions.PositionFromMatrix (localPose);
								goARCameras.transform.position = poseData;
								goARCameras.transform.localRotation = ARUtilityFunctions.QuaternionFromMatrix (localPose);
								//poseData.y = -poseData.y;
								//goARCameras.transform.position = poseData;

								index++;
							}
						
//						if (!arVisible) {
//							// Marker was hidden but now is visible.
//							arVisible = true;
//						}
						}

					} else {
						//for testing the pos data is working
						transform.position = new Vector3 (matrixRawArray [12], matrixRawArray [13], matrixRawArray [14]);
					}
				} else {
					if (goARExperience.activeInHierarchy) {
						goARExperience.SetActive (false);
					}

					if (shouldUpdateTrackingEvents && trackingEventsListeners != null) {
						foreach(TrackingEventListener listener in trackingEventsListeners)
						{
							if (listener != null) {
								listener.OnTrackingLost(this);
							}
							else {
								Debug.LogWarning("OnTrackingLost, but listener is null");
							}
						}
					}
				}
			}
		}
	}
}

