using UnityEngine;
using System.Collections;

public class DSHCamera : MonoBehaviour {

	// Config. out.
	private int _videoWidth = 0;
	private int _videoHeight = 0;
	private int _videoPixelSize = 0;
	private string _videoPixelFormatString = "";


	// Unity objects.
	private GameObject _videoBackgroundMeshGO = null; // The GameObject which holds the MeshFilter and MeshRenderer for the background video, and also the Camera object(s) used to render them. 
	private Color[] _videoColorArray = null; // An array used to fetch pixels from the native side, only if not using native GL texturing.
	private Color32[] _videoColor32Array = null; // An array used to fetch pixels from the native side, only if not using native GL texturing.
	//This gives access to the raw camera data in Unity, when there is no data, this will be null
	public Color32[] VideoColor32Array
	{
		get
		{
			return _videoColor32Array;
		}
	}
	private Texture2D _videoTexture = null;  // Texture object with the video image.
	//This gives access to _videoTexture for the color camera, when there is no data, this will be null
	public Texture2D VideoTexture
	{
		get
		{
			return _videoTexture;
		}
	}
	private Material _videoMaterial = null;  // Material which uses our "VideoPlaneNoLight" shader, and paints itself with _videoTexture0.
	private Camera _videoCamera = null;

	//this gives access to the video background camera, this can be used for enabling, disabling video background rendering, this will be null where the camera gets reset or switched from mono/stereo
	public Camera VideoCamera
	{
		get
		{
			return _videoCamera;
		}
	}

	private GameObject _videoBackgroundMeshGO1 = null; // The GameObject which holds the MeshFilter and MeshRenderer for the background video, and also the Camera object(s) used to render them. 
	private Color[] _videoColorArray1 = null; // An array used to fetch pixels from the native side, only if not using native GL texturing.
	private Color32[] _videoColor32Array1 = null; // An array used to fetch pixels from the native side, only if not using native GL texturing.	
	private Texture2D _videoTexture1 = null;  // Texture object with the video image.
	//This gives access to _videoTexture for the color camera, when there is no data, this will be null (THIS IS FOR THE RIGHT CAMERA DURING STEREO RENDERING)
	public Texture2D VideoTextureRight
	{
		get
		{
			return _videoTexture1;
		}
	}
	private Material _videoMaterial1 = null;  // Material which uses our "VideoPlaneNoLight" shader, and paints itself with _videoTexture0.
	private Camera _videoCamera1 = null;
	//this gives access to the video background camera, this can be used for enabling, disabling video background rendering, this will be null where the camera gets reset or switched from mono/stereo
	//THIS IS FOR THE RIGHT CAMERA DURING STEREO RENDERING
	public Camera VideoCameraRight
	{
		get
		{
			return _videoCamera1;
		}
	}

	GameObject _video = null;
	GameObject _video1 = null;

	[HideInInspector]
	public bool bScreenSpace = false;

	[SerializeField]
	public string cameraParameter = "camera_para";
	[SerializeField]
	public ARTrackable trackable = null;

	[SerializeField]
	public float NearPlane = 0.01f;
	[SerializeField]
	public float FarPlane = 1000.0f;//5.0f;

	[SerializeField]
	private bool stereoRendering = false;

	public bool StereoRendering 
	{
		get 
		{
			return stereoRendering;
		}
		set
		{
			if (stereoRendering != value && Application.isPlaying) {
				//when the application is running, destroy everything and then re-init
				DestroyVideoBackground();
				StopAR();
				stereoRendering = value;
				if (trackable != null) {
					trackable.StereoRendering = stereoRendering;
				}
				Init ();
				InitAR();
			}
			else {
				if (stereoRendering != value) {
					//Application isnt running, no need to destroy anything, just re-int the cameras for trackable
					stereoRendering = value;
					if (trackable != null) {
						trackable.StereoRendering = stereoRendering;
					}
				}
			}

		}
	}

	[SerializeField]
	private bool enableTracking = true;

	public bool EnableTracking
	{
		get
		{
			return enableTracking;
		}

		set
		{
			if (enableTracking != value)
			{
				enableTracking = value;
				if (trackable != null)
				{
					trackable.EnableTracking = enableTracking;
				}
			}
		}
	}

	[SerializeField]
	private bool enableBackgroudVideoCameraRendering = true;

	public bool EnableBackgroudVideoCameraRendering
	{
		get
		{
			return enableBackgroudVideoCameraRendering;
		}
		
		set
		{
			if (enableBackgroudVideoCameraRendering != value)
			{
				enableBackgroudVideoCameraRendering = value;
				ToggleVideoBackground();
				if (trackable != null)
				{
					trackable.EnableVideoRendering = enableBackgroudVideoCameraRendering;
				}
			}
		}
	}

	void ToggleVideoBackground()
	{
		if (_videoCamera != null) {
			_videoCamera.gameObject.SetActive(enableBackgroudVideoCameraRendering);
		}

		if (stereoRendering && _videoCamera1 != null) {
			_videoCamera1.gameObject.SetActive(enableBackgroudVideoCameraRendering);
		}

	}
	
	void Awake() {
		if (trackable != null) {
			//set up the nearplane, farplane, stereo rendering
			trackable.NearPlane = NearPlane;
			trackable.FarPlane = FarPlane;
			trackable.StereoRendering = stereoRendering;
		}
	}

	// Use this for initialization
	void Start () {
		//Init DSH Camera
		Init ();
		//Init the AR
		if (trackable != null) {
			InitAR ();
		}
	}

	void OnApplicationQuit() {
		if (trackable != null) {
			StopAR ();
		}
		Stop ();
		Debug.Log("Application ending after " + Time.time + " seconds");
	}

	void Stop() {
		Debug.Log ("Stopping camera");
		DSHUnityPlugin.StopCamera ();
	}

	void StopAR()
	{
		VisionUnityPlugin.StopAR ();
	}
	
	void InitAR() {
		TextAsset ta;
		byte[] cparam0 = null;
		//byte[] cparam1 = null;
		//byte[] transL2R = null;
		ta = Resources.Load("ardata/" + cameraParameter, typeof(TextAsset)) as TextAsset;

		if (ta == null) {		
            // Error - the camera_para.dat file isn't in the right place			
			Debug.Log("StartAR(): Error: Camera parameters file not found at Resources/ardata/" + cameraParameter + ".bytes");
            return;// (false);
        }
        cparam0 = ta.bytes;
		Debug.Log ("camera parameters byte-size - " + cparam0.Length);
		string dir = Application.streamingAssetsPath;
        string cfg = "";

		if (!string.IsNullOrEmpty(dir) && !string.IsNullOrEmpty(trackable.ARTVisionImageName)) {
					cfg = System.IO.Path.Combine(dir, trackable.ARTVisionImageName);

				}
		Debug.Log("Trackable path - " + cfg);
		VisionUnityPlugin.InitAR (cparam0, cparam0.Length, NearPlane, FarPlane, cfg);
	}

	void Init(){
		//Start DSH Camera
		DSHUnityPlugin.StartCamera ();

		bool okVP = DSHUnityPlugin.getVideoParams(DSHUnityPlugin.HCAMERA_TYPE.COLOR_CAMERA, out _videoWidth, out _videoHeight, out _videoPixelSize, out _videoPixelFormatString);
		if (!okVP)
			return;
		else {
			Debug.Log("Video Params: " + _videoWidth + "x" + _videoHeight + ", pixelSize: " + _videoPixelSize + "bpp,  " + _videoPixelFormatString);
		}

		if (_videoColor32Array == null) {
			//Create Mesh Filter
			//GameObject go = 
			MeshFilter meshfilter = gameObject.GetComponent<MeshFilter>();
			if (meshfilter == null)
			{
				meshfilter = gameObject.AddComponent<MeshFilter>();
			}

			//Create Mesh Renderer
			MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
			if (meshRenderer == null)
			{
				meshRenderer = gameObject.AddComponent<MeshRenderer>();
			}
			CreateVideoBackground ( out _videoColor32Array, out _videoTexture, out _videoMaterial, meshfilter, meshRenderer, "video", 5, out _videoCamera, 0);
			if (_videoCamera == null) {
				Debug.LogError("issue creating video camera");
			}
			else {
				if (enableBackgroudVideoCameraRendering) {
					_videoCamera.gameObject.SetActive(enableBackgroudVideoCameraRendering);
				}
			}
		}

		if (stereoRendering && _videoColor32Array1 == null) {

			GameObject go = new GameObject("right");
			//Create Mesh Filder
			MeshFilter meshfilter = go.AddComponent<MeshFilter>();
			//Create Mesh Renderer
			MeshRenderer meshRenderer = go.AddComponent<MeshRenderer>();
			CreateVideoBackground ( out _videoColor32Array1, out _videoTexture1, out _videoMaterial1, meshfilter, meshRenderer, "video1", 5, out _videoCamera1, 1);
			if (_videoCamera1 == null) {
				Debug.LogError("issue creating right video camera");
			}
			else {
				if (enableBackgroudVideoCameraRendering) {
					_videoCamera1.gameObject.SetActive(enableBackgroudVideoCameraRendering);
				}
			}
		}
		ToggleVideoBackground ();
	}


	private void CreateVideoBackground(out Color32[] videoColor32Array, out Texture2D videoTexture, out Material videoMaterial, MeshFilter meshfilter, MeshRenderer meshRenderer, string name, int layer, out Camera videoCamera, int camIndex = 0)
	{
		gameObject.layer = 5;
		
		videoColor32Array = new Color32[_videoWidth * _videoHeight];
		videoTexture = new Texture2D (_videoWidth, _videoHeight, TextureFormat.RGB24, false);
		videoTexture.hideFlags = HideFlags.HideAndDontSave;
		videoTexture.filterMode = FilterMode.Bilinear;
		videoTexture.wrapMode = TextureWrapMode.Clamp;
		videoTexture.anisoLevel = 0;
		
		// Create a material tied to the texture.
		Shader shaderSource = Shader.Find("VideoPlaneNoLight");

		videoMaterial = new Material(shaderSource);
		videoMaterial.hideFlags = HideFlags.HideAndDontSave;
		videoMaterial.mainTexture = videoTexture;
		//videoMaterial.color = Color.black;
		//_videoMaterial.color = Color.red;
		//creating mesh
		Mesh m = new Mesh();
		m.Clear();
		
		float r = 1.0f;
		
		m.vertices = new Vector3[] { 
			new Vector3 (-r, -r, 0.5f), 
			new Vector3 (r , -r, 0.5f), 
			new Vector3 (r , r , 0.5f),
			new Vector3 (-r, r , 0.5f),
		};
		
		
		m.normals = new Vector3[] { 
			new Vector3(0.0f, 0.0f, 1.0f), 
			new Vector3(0.0f, 0.0f, 1.0f), 
			new Vector3(0.0f, 0.0f, 1.0f),
			new Vector3(0.0f, 0.0f, 1.0f),
		};
		
		float u1 = 0.0f;
		float u2 = 1.0f;
		
		float v1 = 0.0f;
		float v2 = 1.0f;
		
		m.uv = new Vector2[] { 
			new Vector2(u1, v1), 
			new Vector2(u2, v1), 
			new Vector2(u2, v2),
			new Vector2(u1, v2),
		};
		
		m.triangles = new int[] { 
			2, 1, 0,
			3, 2, 0
		};
		
		m.Optimize();
		
		meshfilter.mesh = m;
		
		
		meshRenderer.castShadows = false;
		meshRenderer.receiveShadows = false;
		meshRenderer.material = videoMaterial;
		GetComponent<Renderer>().material = videoMaterial;
		
		//create video background camera
		CreateVideoBackgroundCamera(name, layer, out videoCamera, camIndex);
	}
	
	// Creates a GameObject holding a camera with name 'name', which will render layer 'layer'.
	private GameObject CreateVideoBackgroundCamera(string name, int layer, out Camera vbc, int camIndex = 0)
	{
		// Create new GameObject to hold camera.
		GameObject vbcgo = new GameObject(name);
		if (vbcgo == null) {
			Debug.Log("Error: CreateVideoBackgroundCamera cannot create GameObject.");
			vbc = null;
			return null;
		}
		//vbgo.layer = layer; // Belongs in the background layer.
		
		vbc = vbcgo.AddComponent<Camera>();
		if (vbc == null) {
			Debug.Log("Error: CreateVideoBackgroundCamera cannot add Camera to GameObject.");
			return null;
		}
		
		// Camera at origin, orthographic projection.
		vbc.orthographic = true;
		vbc.projectionMatrix = Matrix4x4.identity;
		//if (ContentRotate90) vbc.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(90.0f, Vector3.back), Vector3.one) * vbc.projectionMatrix;
		//vbc.projectionMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180.0f, Vector3.back), new Vector3(1, 1, 1)) * vbc.projectionMatrix;
		//vbc.projectionMatrix = vbc.projectionMatrix * Matrix4x4.Scale (new Vector3 (-1, 1, 1));
		vbc.projectionMatrix = vbc.projectionMatrix * Matrix4x4.Scale (new Vector3 (1, -1, 1));

		vbc.projectionMatrix = Matrix4x4.Ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f) * vbc.projectionMatrix;
		vbc.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		vbc.transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
		vbc.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
		
		// Clear everything as the video is the background.
		//vbc.clearFlags = CameraClearFlags.SolidColor;
		//vbc.backgroundColor = Color.black;
		vbc.clearFlags = CameraClearFlags.SolidColor;

		if (vbc.gameObject.GetComponent<CameraRender> () == null) {
			vbc.gameObject.AddComponent<CameraRender>();
		}
		
		// The background camera displays only the background layer
		vbc.cullingMask = 1 << layer;
		
		// Renders after the clear camera but before foreground cameras
		vbc.depth = 1;
		
		// Finally: having done all this setup, if video background isn't actually wanted, disable the camera.
		vbc.enabled = true;//UseVideoBackground;

		if (!stereoRendering) {
			vbc.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
			_video = vbcgo;
		} else {
			if (camIndex == 0) {
				vbc.rect = new Rect (0.0f, 0.0f, 0.5f, 1.0f);
				_video = vbcgo;
			} else {
				vbc.rect = new Rect (0.5f, 0.0f, 0.5f, 1.0f);
				_video1 = vbcgo;
			}
		}
		//vbc.pixelRect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);//_videoWidth, _videoHeight);
		//vbc.tag = name;
		return vbcgo;
	}

	// Update is called once per frame
	void Update () {
		if (enableBackgroudVideoCameraRendering) {
			if (_videoColor32Array != null) {
			
				bool updatedTexture = DSHUnityPlugin.GetColorCamData (_videoColor32Array);
				if (updatedTexture) {
					//Debug.Log("Updating texture VP");
					_videoTexture.SetPixels32 (_videoColor32Array);
					_videoTexture.Apply ();

					if (stereoRendering) {
						_videoTexture1.SetPixels32 (_videoColor32Array);
						_videoTexture1.Apply ();
					}
				}
			}
		}

		if (trackable != null && enableTracking) {
			VisionUnityPlugin.UpdateAR ();
		}
		//GetComponent<Renderer> ().material.mainTexture = _videoTexture;
	}

	//Clean up
	private void DestroyVideoBackground()
	{
		bool ed = Application.isEditor;

		if (_videoMaterial != null) {
			//if (ed) DestroyImmediate(_videoMaterial);
			//else 
			Destroy(_videoMaterial);
			_videoMaterial = null;
		}

		if (_videoTexture != null) {
			//if (ed) DestroyImmediate(_videoTexture);
			//else
			Destroy(_videoTexture);
			_videoTexture = null;
		}

		if (_videoColorArray != null) _videoColorArray = null;
		if (_videoColor32Array != null) _videoColor32Array = null;
		if (_videoBackgroundMeshGO != null) {
			if (ed) DestroyImmediate(_videoBackgroundMeshGO);
			else Destroy(_videoBackgroundMeshGO);
			_videoBackgroundMeshGO = null;
		}

		if (gameObject.GetComponent<MeshFilter> () != null) {
			Destroy(gameObject.GetComponent<MeshFilter>());
		}

		GameObject go = GameObject.Find ("video");
		if (go != null) {
			Destroy (go);
		} else {
			if (_video != null) {
				Destroy(_video);
				_video = null;
			}
		}

		if (_videoCamera != null) {
			Destroy(_videoCamera);
			_videoCamera = null;
		}

		if (stereoRendering) {
			if (_videoMaterial1 != null) {
				//if (ed) DestroyImmediate(_videoMaterial1);
				//else
				Destroy(_videoMaterial1);
				_videoMaterial1 = null;
			}
			
			if (_videoTexture1 != null) {
				//if (ed) DestroyImmediate(_videoTexture1);
				//else
				Destroy(_videoTexture1);
				_videoTexture1 = null;
			}
			
			if (_videoColorArray1 != null) _videoColorArray1 = null;
			if (_videoColor32Array1 != null) _videoColor32Array1 = null;
			if (_videoBackgroundMeshGO1 != null) {
				if (ed) DestroyImmediate(_videoBackgroundMeshGO1);
				else Destroy(_videoBackgroundMeshGO1);
				_videoBackgroundMeshGO1 = null;
			}

			GameObject go1 = GameObject.Find("video1");
			if (go1 != null) {
				Destroy(go1);
			}
			else if (_video1 != null) {
				Destroy(_video1);
				_video1 = null;
			}

			if (_videoCamera1 != null) {
				Destroy(_videoCamera1);
				_videoCamera1 = null;
			}



			GameObject right = GameObject.Find("right");
			if (right != null) {
				Destroy(right);
			}
		}
		Resources.UnloadUnusedAssets();
	}
}
