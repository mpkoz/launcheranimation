﻿using UnityEngine;
using System.Collections;

public class VisionUnityPlugin {
	
	public static bool InitAR(byte[] cparaBuff, int cparaBuffLen, float nearPlane, float farPlane, string markerPath)
	{
		return VisionUnityAbstraction.HTrackerStart (cparaBuff, cparaBuffLen, nearPlane, farPlane, markerPath);
	}

	public static bool UpdateAR()
	{
		return VisionUnityAbstraction.HTrackerUpdate ();
	}

	public static bool GetTransformation(float[] transformationMatrix)
	{
		return VisionUnityAbstraction.HTrackerGetTrackableTransformation(transformationMatrix);
	}

	public static bool StopAR()
	{
		return VisionUnityAbstraction.HTrackerStop ();
	}

	public static void GetProjectionMatrix(float[] projMatrix)
	{
		VisionUnityAbstraction.HTrackerGetProjectionMatrix (projMatrix);
	}
	
	public static bool GetOpticalParameters(byte[] opticalBuff, int paramLen, out float fov, out float aspect, float[] m, float[] p)
	{
		return VisionUnityAbstraction.HTrackerGetOpticalParameters (null, opticalBuff, paramLen, out fov, out aspect, m, p);
	}
}
