
using System;
using UnityEngine;

public class CameraRender : MonoBehaviour
{
	void OnPreRender() {
		//Debug.Log ("OnPreRender()");
		GL.SetRevertBackfacing (true);
	}
	void OnPostRender() {
		//Debug.Log ("OnPostRender()");
		GL.SetRevertBackfacing (false);
	}

}


