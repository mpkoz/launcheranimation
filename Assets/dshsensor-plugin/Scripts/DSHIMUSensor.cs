﻿using UnityEngine;
using System.Collections;

public class DSHIMUSensor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DSHUnityPlugin.StartIMU ();
	}

	void Stop () {
		Debug.Log ("Stopping IMU");
		DSHUnityPlugin.StopIMU ();
	}

	void OnApplicationQuit() {
		Stop ();
	}
	
	// Update is called once per frame
	void Update () {
		IMUData data;

		if (DSHUnityPlugin.GetIMUData (out data)) {
			//Debug.Log ("IMU gyro Data -- " + data.Gyro);
			//Debug.Log ("IMU eul Data -- " + data.Eul);
			//transform.localRotation = data.Quat;
			transform.eulerAngles += new Vector3(-data.Gyro.x, -data.Gyro.y * 1.5f, 0);
		} else {
			Debug.LogWarning("IMU data invalid");
		}
	}
}
