using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ARCamera))] 
public class ARCameraEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		ARCamera arCamera = (ARCamera)target;
		if (arCamera == null) {
			Debug.LogError("ARCamera is null");
			return;
		}
		
		EditorGUILayout.Separator();
		arCamera.OpticalSeeThrough = EditorGUILayout.Toggle ("Optical see-through", arCamera.OpticalSeeThrough);

		if (arCamera.OpticalSeeThrough == true) {
			arCamera.OpticalParamFile = EditorGUILayout.TextField ("Optical Parameters", arCamera.OpticalParamFile);

			//if (arCamera.OpticalParamFile == string.Empty)
//			{
//				arCamera.PositionOffset = EditorGUILayout.Vector3Field ("Position Offset", arCamera.PositionOffset);
//				arCamera.RotationOffset = EditorGUILayout.Vector3Field ("Rotation Offset", arCamera.RotationOffset);
//			}

		}
	}
}
