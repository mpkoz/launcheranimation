using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DSHCamera))] 
public class DSHCameraEditor : Editor 
{
    public override void OnInspectorGUI()
    {
		DSHCamera dshCamera = (DSHCamera)target;
		if (dshCamera == null) {
			Debug.LogError("DSH Camera is null");
			return;
		}

		EditorGUILayout.Separator();

		dshCamera.cameraParameter = EditorGUILayout.TextField ("Camera Parameters", dshCamera.cameraParameter);
		dshCamera.NearPlane = EditorGUILayout.FloatField ("Near Plane", dshCamera.NearPlane);
		dshCamera.FarPlane = EditorGUILayout.FloatField ("Far Plane", dshCamera.FarPlane);
		dshCamera.EnableBackgroudVideoCameraRendering = EditorGUILayout.Toggle ("Enable VideoBackground Rendering", dshCamera.EnableBackgroudVideoCameraRendering);
		dshCamera.StereoRendering = EditorGUILayout.Toggle ("Stereo Rendering", dshCamera.StereoRendering);
		dshCamera.EnableTracking = EditorGUILayout.Toggle ("Enable Tracking", dshCamera.EnableTracking);
		dshCamera.trackable = EditorGUILayout.ObjectField ("Trackable", dshCamera.trackable, typeof(ARTrackable), true) as ARTrackable;
    }
}
