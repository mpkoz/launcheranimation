﻿using UnityEngine;
using System.Collections;

public class DSHReticle : MonoBehaviour {

	public Transform arcameras;
	private float offsetInfrontOfObject = -0.02f;
	private Vector3 orgLocalScale;

	private float orgReticleDistanceFromCamera;
	private float scalefactor = 1.0f;
	// Use this for initialization
	void Start () {
		orgLocalScale = transform.localScale;
		orgReticleDistanceFromCamera = Vector3.Magnitude(transform.position - arcameras.position);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 fwd = arcameras.TransformDirection(Vector3.forward);
		RaycastHit hit;
		Ray ray = new Ray (arcameras.position, fwd);
		Vector3 pos = transform.localPosition;
		if (Physics.Raycast (ray, out hit, 1000.0f)) {
			//Vector3 pos = transform.position;
			float distance = Vector3.Magnitude(hit.point - arcameras.position) - offsetInfrontOfObject;
			pos.z = arcameras.TransformDirection(Vector3.forward * distance).z;
			transform.localPosition = pos;
		} else {
			transform.localPosition = new Vector3(pos.x,pos.y,1.0f);//arcamera.WorldToScreenPoint (arcamera.transform.position - 10.0f * arcamera.transform.forward.normalized) ;
		}



		float newReticleDistanceFromCamera = Vector3.Magnitude(transform.position - arcameras.position);
		scalefactor = newReticleDistanceFromCamera / orgReticleDistanceFromCamera;
		//Calculate the scale change for z change
		transform.localScale = orgLocalScale * scalefactor;
	}
}