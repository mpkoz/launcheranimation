﻿using UnityEngine;
using System.Collections;

public class ARCamera : MonoBehaviour {
	private DSHCamera dshCamera = null;

	[SerializeField]
	private bool opticalSeeThrough = false;
	public bool OpticalSeeThrough
	{
		get 
		{
			return opticalSeeThrough;
		}
		set
		{
			if (opticalSeeThrough != value)
			{
				opticalSeeThrough = value;
				//trackable = gameObject.GetComponentsInParent<ARTrackable>(true);
				GameObject goDSH = GameObject.Find("DSHCamera");
				if (goDSH != null)
				{
					dshCamera = goDSH.GetComponent<DSHCamera>();
					if (dshCamera != null)
					{
						//dshCamera.EnableBackgroudVideoCameraRendering = !opticalSeeThrough;
					}
				}
			}
		}
	}

	[SerializeField]
	private string opticalParamFile = string.Empty;
	public string OpticalParamFile
	{
		get 
		{
			return opticalParamFile;
		}
		set
		{
			if (opticalParamFile != value)
			{
				opticalParamFile = value;
			}
		}
	}

	[SerializeField]
	private Vector3 positionOffset = Vector3.zero;
	[HideInInspector]
	public Vector3 PositionOffset
	{
		get 
		{
			return positionOffset;
		}
		set
		{
			if (positionOffset != value)
			{
				positionOffset = value;
			}
		}
	}

	[SerializeField]
	private Vector3 rotationOffset = Vector3.zero;
	[HideInInspector]
	public Vector3 RotationOffset
	{
		get 
		{
			return rotationOffset;
		}
		set
		{
			if (rotationOffset != value)
			{
				rotationOffset = value;
			}
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnPreRender() {
		//Debug.Log ("OnPreRender()");
		GL.SetRevertBackfacing (true);
	}
	void OnPostRender() {
		//Debug.Log ("OnPostRender()");
		GL.SetRevertBackfacing (false);
	}
}
