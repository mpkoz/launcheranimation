﻿// ------------------------------------------------------------------------------
//  Created by:
//  NAVEEN ANAND GUNALAN
// ------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using UnityEngine;

public static class DSHUnityAbstraction
{
	// The name of the external library containing the native functions
	#if UNITY_EDITOR
	private const string LIBRARY_NAME = "libsmarthelmet";	

	#else
	private const string LIBRARY_NAME = "smarthelmet";
	#endif
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	public static extern bool HCameraStart(int direction, int preset, int outFormat, bool enableGrayScale = true);
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HCameraGetParams(int direction, IntPtr width, IntPtr height);
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HCameraUpdatePreset(int direction, int preset, int outFormat);
	//for version 1.0.4.*
	//public static extern bool HCameraUpdatePreset(int direction, int preset);
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HCameraGetData(int direction, IntPtr data);
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HCameraStop(int direction);
	
	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern void HCameraGetGrayScaleData(int direction, IntPtr data);

	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HIMUStart();

	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HIMUGetData([MarshalAs(UnmanagedType.LPArray, SizeConst=18)] float[] data);

	[DllImport(LIBRARY_NAME, CallingConvention=CallingConvention.Cdecl)]
	[return: MarshalAsAttribute(UnmanagedType.I1)]
	public static extern bool HIMUStop();
}


