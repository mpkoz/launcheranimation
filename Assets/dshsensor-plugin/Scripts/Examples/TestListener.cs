using System;
using UnityEngine;

	public class TestListener : MonoBehaviour,  TrackingEventListener
	{
		public void Awake() {
			ARTrackable.AddTrackingEventListener (this);
		}

		public void OnTrackingFound(ARTrackable obj) {
			Debug.Log ("Tracking found");
		}
		public void OnTrackingLost(ARTrackable obj) {
			Debug.Log ("Tracking lost");
		}
	}

