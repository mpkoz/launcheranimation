using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

public static class DSHUnityPlugin {

	public enum HCAMERA_TYPE
	{
		COLOR_CAMERA = 1 << 0,
		Z_CAMERA = 1 << 1,
		IR_CAMERA = 1 << 2,
		LWIR_CAMERA = 1 << 5,
		
	};

	public enum HDIRECTION
	{
		FRONT = 1 << 0,
		REAR = 1 << 1,
	}

	public enum HCAMERA_FORMAT
	{
		ERROR = -1,      // Bad image format
		YUV_I422 = 0,    // YUY'V 422 interleaved format
		BGRA_D888X = 1,  // BGRA deinterleaved pre-multiplied alpha format
		RGB_D888 = 2,    // RGB deinterleaved format
		LUM_D16 = 3,     // 16 bit luminance deinterleaved format
		LUM_D8 = 4,      // 8 bit luminance deinterleaved format
	}

	//for tracking
	public static bool enableGrayScale = true;
	
	
	
	//DO NOT CHANGE THESE VALUES, this will result in crash at this point
	private static int CAMERA_WIDTH = 1920;
	private static int CAMERA_HEIGHT = 1080;
	//DO NOT CHANGE THESE VALUES, this will result in crash at this point
	private static int DEPTH_CAMERA_WIDTH = 320;
	private static int DEPTH_CAMERA_HEIGHT = 240;
	
	private static int LWIR_CAMERA_WIDTH = 160;
	private static int LWIR_CAMERA_HEIGHT = 120;
	//DO NOT CHANGE THESE VALUES, this will result in crash at this point
	private static int Z_SIZE_IN_BYTES = 2;
	private static int IR_SIZE_IN_BYTES = 1;
	
	private static HCAMERA_TYPE camera_type;

	public static void StartCamera()
	{
		int direction = 1<<0;
		int video_camera_preset = 1;
		int output_format = 2;
		Debug.Log ("Camera type -- " + direction);
		bool ok = DSHUnityAbstraction.HCameraStart(direction, video_camera_preset, output_format, true);
		if (!ok) {
			Debug.LogWarning("Unity abstractions not started correctly, Realsense camera feeds may not work");
		}
	}

	public static void StopCamera()
	{
		int direction = 1;
		bool ok = DSHUnityAbstraction.HCameraStop(direction);
		if (!ok) {
			Debug.LogWarning("Realsense camera didnt stop properly");
		}
	}

	public static bool GetColorCamData([In, Out]Color32[] colors32)
	{
		//Debug.Log ("Grabbing data from arwUpdateTexture32_ioa");
		bool ok = false;
		byte[] colData = new byte[CAMERA_WIDTH * CAMERA_HEIGHT * 3];

		GCHandle handle = GCHandle.Alloc(colData, GCHandleType.Pinned);
		//GCHandle handle = GCHandle.Alloc(colors32, GCHandleType.Pinned);

		IntPtr address = handle.AddrOfPinnedObject();
		ok = DSHUnityAbstraction.HCameraGetData((int)HCAMERA_TYPE.COLOR_CAMERA, address);
		if (ok == true) {
			int x = 0;
			for (int i = 0; i < CAMERA_HEIGHT; ++i)
			{
				for (int j = 0; j < CAMERA_WIDTH; ++j)
				{
					int index = i* CAMERA_WIDTH + j;
					//colors32[index] = new Color32(125, 0, 0, 125);
					colors32[index].r = colData[x++];
					colors32[index].g = colData[x++];
					colors32[index].b = colData[x++];
					colors32[index].a = 255;
				}
			}
			//Debug.Log ("Color cam Grab Success -- " + x);
		} else {
			//Debug.Log ("Color cam Grab failed");
		}

		handle.Free();
		return ok;
	}

	public static bool GetNonColorCamData([In, Out]Color[] colors, HCAMERA_TYPE camType)
	{
		camera_type = camType;
		int multiplier = IR_SIZE_IN_BYTES;
		if (camType == HCAMERA_TYPE.Z_CAMERA) {
			multiplier = Z_SIZE_IN_BYTES;
		}
		byte[] lumi = new byte[DEPTH_CAMERA_WIDTH * DEPTH_CAMERA_HEIGHT * multiplier];
		
		bool ok = false;
		GCHandle handle = GCHandle.Alloc(lumi, GCHandleType.Pinned);
		IntPtr address = handle.AddrOfPinnedObject();

		ok = DSHUnityAbstraction.HCameraGetData ((int)camType, address);
		if (ok == true) {
			//Debug.Log ("Non Color cam Grab Success");
		} else {
			//Debug.Log ("Non Color cam Grab failed");
		}
		handle.Free();
		
		for (int i = 0; i < DEPTH_CAMERA_HEIGHT; ++i) 
		{
			for (int j = 0; j < DEPTH_CAMERA_WIDTH; ++j)
			{
				int index = (i * DEPTH_CAMERA_WIDTH) + j;
				int z_index = multiplier * index;
				colors[index].r = colors[index].g = colors[index].b = (float)(1.0f * (lumi[z_index])) / 255.0f;
				colors[index].a = 1.0f;
			}
		}
		return ok;
	}

	public static bool getVideoParams(HCAMERA_TYPE cam_type, out int width, out int height, out int pixelSize, out String pixelFormatString)
	{
		camera_type = cam_type;
		bool ok = true;
		//ideally we should be grabbing these values from the device, hardcoded for now...
		//StringBuilder sb = new StringBuilder(128);
		//if (Application.platform == RuntimePlatform.IPhonePlayer) ok = ARNativePluginStatic.arwGetVideoParams(out width, out height, out pixelSize, sb, sb.Capacity);
		//else 
		//ok = ARNativePlugin.arwGetVideoParams(out width, out height, out pixelSize, sb, sb.Capacity);
		if (cam_type == HCAMERA_TYPE.COLOR_CAMERA) {
			//int w = CAMERA_WIDTH, h = CAMERA_HEIGHT;
			int[] w = new int[1];
			int[] h = new int[1];
			width = CAMERA_WIDTH;
			height = CAMERA_HEIGHT;
			bool okParams = true;
			GCHandle handleW = GCHandle.Alloc(w, GCHandleType.Pinned);
			IntPtr addressW = handleW.AddrOfPinnedObject();
			GCHandle handleH = GCHandle.Alloc(h, GCHandleType.Pinned);
			IntPtr addressH = handleH.AddrOfPinnedObject();
			//Debug.Log ("Cam type is -- " + (int)camType);
			okParams = DSHUnityAbstraction.HCameraGetParams ((int)cam_type, addressW, addressH);
			if (okParams)
			{
				if (w[0]!=-1 && h[0]!= -1)
				{
					width = w[0];
					height = h[0];
					CAMERA_WIDTH = width;
					CAMERA_HEIGHT = height;
				}
			}

			handleW.Free();
			handleH.Free();
			
			pixelFormatString = "GL_RGB";
			pixelSize = 24;
		} else if (cam_type == HCAMERA_TYPE.LWIR_CAMERA) {
			width = LWIR_CAMERA_WIDTH;
			height = LWIR_CAMERA_HEIGHT;
			
			int w = LWIR_CAMERA_WIDTH, h = LWIR_CAMERA_HEIGHT;
			bool okParams = true;
			GCHandle handleW = GCHandle.Alloc(w, GCHandleType.Pinned);
			IntPtr addressW = handleW.AddrOfPinnedObject();
			GCHandle handleH = GCHandle.Alloc(h, GCHandleType.Pinned);
			IntPtr addressH = handleH.AddrOfPinnedObject();
			//Debug.Log ("Cam type is -- " + (int)camType);
			okParams = DSHUnityAbstraction.HCameraGetParams ((int)cam_type, addressW, addressH);
			handleW.Free();
			handleH.Free();
			
			if (okParams)
			{
				if (w!=-1 && h!= -1)
				{
					width = w;
					height = h;
				}
			}
			pixelFormatString = "GL_RGB";
			pixelSize = 24;
		}
		else {
			width = DEPTH_CAMERA_WIDTH;
			height = DEPTH_CAMERA_HEIGHT;
			
			int w = DEPTH_CAMERA_WIDTH, h = DEPTH_CAMERA_HEIGHT;
			bool okParams;
			GCHandle handleW = GCHandle.Alloc(w, GCHandleType.Pinned);
			IntPtr addressW = handleW.AddrOfPinnedObject();
			GCHandle handleH = GCHandle.Alloc(h, GCHandleType.Pinned);
			IntPtr addressH = handleH.AddrOfPinnedObject();
			//Debug.Log ("Cam type is -- " + (int)camType);
			okParams = DSHUnityAbstraction.HCameraGetParams ((int)cam_type, addressW, addressH);
			handleW.Free();
			handleH.Free();

			if (okParams) {
				if (w!=-1 && h!= -1)
				{
					width = w;
					height = h;
				}
			}

			pixelFormatString = "GL_LUMINANCE";
			if (cam_type == HCAMERA_TYPE.Z_CAMERA) {
				pixelSize = 8 * Z_SIZE_IN_BYTES;
			} else {
				pixelSize = 8 * IR_SIZE_IN_BYTES;
			}
		}
		return ok;
	}

	public static bool StartIMU() {
		return DSHUnityAbstraction.HIMUStart ();
	}

	public static bool StopIMU() {
		return DSHUnityAbstraction.HIMUStop ();
	}

	public static bool GetIMUData(out IMUData imuData) {
		imuData = new IMUData ();
		float[] data = new float[18];
		bool status = DSHUnityAbstraction.HIMUGetData (data);

		if (status) {
			imuData.SetNativeData (data);
		} else {
			Debug.LogWarning("Issues in grabbing data from IMU");
		}

		return status;
	}

	public static void GetProjectionMatrix(float[] projMatrix)
	{
		VisionUnityAbstraction.HTrackerGetProjectionMatrix (projMatrix);
	}
}
