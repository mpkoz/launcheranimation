﻿using UnityEngine;
using System.Collections;

public class LauncherPlacement : MonoBehaviour {

	public GameObject launcherButton;
	public GameObject boxes;
	public GameObject convergenceSpace;

	// Use this for initialization
	void Start () {
		                                                   
	}
	
	// Update is called once per frame
	void Update () {
		
		this.transform.position = new Vector3 (launcherButton.transform.position.x, launcherButton.transform.position.y - 1.5f, launcherButton.transform.position.z);
		boxes.transform.position = new Vector3 (launcherButton.transform.position.x - 0.4f, launcherButton.transform.position.y - 2.9f, 8.0f);
		this.transform.localRotation = new Quaternion (-convergenceSpace.transform.rotation.x, -convergenceSpace.transform.rotation.y, 0.0f, convergenceSpace.transform.rotation.w);
		boxes.transform.localRotation = new Quaternion (-convergenceSpace.transform.rotation.x, -convergenceSpace.transform.rotation.y, 0.0f, convergenceSpace.transform.rotation.w);
	}
}
