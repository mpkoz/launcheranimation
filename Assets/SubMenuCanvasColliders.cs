﻿using UnityEngine;
using System.Collections;

public class SubMenuCanvasColliders : MonoBehaviour {

	// Use this for initialization
	public GameObject reticleFollower;
	public GameObject buttonWatcher;
	public Animator subMenuAnimator;
	public GameObject button;
	//public Animator subMenuAnimator;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (reticleFollower.GetComponent<ReticleFollower> ().getItem () == this.transform && reticleFollower.GetComponent<ReticleFollower>().getIsInItem() == true) {

			buttonWatcher.GetComponent<ButtonWatcher> ().setOpenMenu(false);
			subMenuAnimator.SetTrigger ("menuClose");
			subMenuAnimator.ResetTrigger ("menuOpen");
			button.GetComponent<BoxCollider> ().enabled = true;


		}
	}
}

/*
if (currentObject.transform.GetChild(1)!= null) {
			print ("CHILD 1: " + currentObject.transform.GetChild (1).GetComponent<Text> ().text);
			currentText.text = currentObject.transform.GetChild (1).GetComponent<Text> ().text;
		}


public void ShowAppWindow(){

		popUpTitle.GetComponent<Text> ().text = currentText.text;

	}
*/
