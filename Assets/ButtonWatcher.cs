using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonWatcher : MonoBehaviour {

	public GameObject convergenceSpace;
	public GameObject launcherButton;
	public Animator menuAnimator;
	public ReticleFollower reticleFollower;
	public Animator scrollAnimator;
	public Animator subMenuAnimator;
	public Animator tileAnimator;
	public GameObject buttons;
	public GameObject menuButtons;
	public GameObject tiles;
	public GameObject spotlight;
	public GameObject backBlock;
	public GameObject popUpTitle;
	public Image micButton;
	public Sprite audioSelectable;
	public Sprite audioOn;


	private bool isMinimized;
	private bool openMenu;
	private bool scrolledUp;
	private bool subMenuHoveredOn;
	private bool tileIsHoveredOn;
	private bool drawIsHoveredOn;
	private GameObject currentObject;
	private GameObject oldObject;
	private Transform currentTransform;
	private Color thisColor;
	private GameObject currentButton;
	private GameObject oldButton;
	private GameObject currentMenuButton;
	private GameObject oldMenuButton;
	private Color buttonColor;
	private BoxCollider[] buttonColliderArray;
	private BoxCollider[] tileColliderArray;
	private Transform[] individualTiles;
	private GameObject[] borders;
	private GameObject currentTile;
	private bool firstOpen;
	private bool hide1;
	private bool hide2;
	private bool hide3;
	private GameObject[] quads;
	private string currentText;
	private GameObject oldTile;

	//spotlight effect switches between layermasks
	public LayerMask settingsLayer;
	public LayerMask remoteExpertLayer;
	public LayerMask cameraLayer;
	public LayerMask thermalLayer;
	public LayerMask third1Layer;
	public LayerMask galleryLayer;
	public LayerMask viewerLayer;
	public LayerMask third2Layer;
	public LayerMask third3Layer;
	public LayerMask third4Layer;
	public LayerMask third5Layer;
	public LayerMask third6Layer;
	public LayerMask third7Layer;

	private float convergenceSpaceQuaternionAbs;


	// Use this for initialization
	void Start () {
		isMinimized = true;
		tileIsHoveredOn = false;
		scrolledUp = true;
		openMenu = false;
		subMenuHoveredOn = false;
		firstOpen = true;
		individualTiles = tiles.GetComponentsInChildren<Transform>();



	}

	// Update is called once per frame
	void Update () {
		quads = GameObject.FindGameObjectsWithTag ("Quads");

		//gets current item from reticle follower script
		currentTransform = reticleFollower.getItem ();
		currentObject = currentTransform.gameObject;

		print ("CURRENT OBJECT: " + currentObject.name);
		if (oldObject != null) {
			print ("OLD OBJECT: " + oldObject.name);
		}
			
		//most animations in this project are binary, ie have an A action and a B action 
		//that end where the other starts, loop. this block of code basically says that if
		//a new object is hovered or selected on, to run the B animation of the previous 
		//object. essentially, restarts the loop. 
		if (oldObject != null) {
			if (currentObject.name != oldObject.name && currentObject.tag != "Selectable") {
				if (oldObject.tag == "Hoverable") {
					if (!openMenu) {
						oldObject.GetComponent<Animator> ().SetTrigger ("hoverOff");
						oldObject.GetComponent<Animator> ().ResetTrigger ("hoverOn");
						if (oldButton != null) {
							oldButton.SetActive (false);
						}
					} else {
						oldObject.GetComponent<Animator> ().SetTrigger ("menuDwellHoverOff");
						oldObject.GetComponent<Animator> ().ResetTrigger ("menuDwellHoverOn");
						if (oldMenuButton != null) {
							//buttonColor = currentButton.GetComponent<Image> ().color;
							currentMenuButton.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
							currentMenuButton.transform.GetChild (0).transform.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
							oldMenuButton.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
							oldMenuButton.transform.GetChild (0).transform.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
							oldMenuButton.SetActive (false);
						}
					}

				}
			}
		}
			
		//this is a work in progress, has no effect on current state of project
		convergenceSpaceQuaternionAbs = Mathf.Abs(convergenceSpace.transform.rotation.x);
		if (convergenceSpaceQuaternionAbs > 0.16f && isMinimized) {
			launcherButton.GetComponent<Animator> ().SetTrigger ("enterLoop");
			launcherButton.GetComponent<Animator> ().SetTrigger ("fadeIn");
			launcherButton.GetComponent<Animator> ().ResetTrigger ("fadeOut");
		} else if (convergenceSpaceQuaternionAbs < 0.1f && isMinimized) {
			launcherButton.GetComponent<Animator> ().SetTrigger ("fadeOut");
			launcherButton.GetComponent<Animator> ().ResetTrigger ("fadeIn");
		}


		//MINIMIZE/MAXIMIZE LAUNCHER MENU CONTROLLER
		if (isMinimized) {
			menuAnimator.SetTrigger ("Minim");
			menuAnimator.ResetTrigger("Maxim");
			for (int i = 0; i < quads.Length; i++) {
				quads [i].GetComponent<RawImage>().enabled = false;
			}
			backBlock.SetActive (true);
			for (int i = 0; i < tiles.transform.childCount; i++) {
				tiles.transform.GetChild (i).gameObject.GetComponent<Animator> ().SetTrigger ("hoverOff");
				tiles.transform.GetChild (i).gameObject.GetComponent<Animator> ().ResetTrigger ("hoverOn");
			}
		} else {
			menuAnimator.SetTrigger ("Maxim");
			menuAnimator.ResetTrigger("Minim");
			launcherButton.SetActive (false);
			backBlock.SetActive (false);
		}
			
		 

		//MAIN MENU TILE ANIMATION CONTROLLER
		if (tileIsHoveredOn) {
			if (currentObject.GetComponent<Animator> () != null && currentObject.tag == "Hoverable") {

				//this block of code tells what button to appear when a corresponding tile is hovered on
				if (currentObject.name == "Settings") {
					spotlight.GetComponent<Light> ().cullingMask = settingsLayer;
					showButton (0);
				} else if (currentObject.name == "RemoteExpert") {
					spotlight.GetComponent<Light> ().cullingMask = remoteExpertLayer;
					showButton (1);
				} else if (currentObject.name == "Camera") {
					spotlight.GetComponent<Light> ().cullingMask = cameraLayer;
					showButton (2);
				} else if (currentObject.name == "Thermal") {
					spotlight.GetComponent<Light> ().cullingMask = thermalLayer;
					showButton (3);
				} else if (currentObject.name == "3rdParty1") {
					spotlight.GetComponent<Light> ().cullingMask = third1Layer;
					showButton (4);
				} else if (currentObject.name == "Gallery") {
					spotlight.GetComponent<Light> ().cullingMask = galleryLayer;
					showButton (5);
				} else if (currentObject.name == "4DViewer") {
					spotlight.GetComponent<Light> ().cullingMask = viewerLayer;
					showButton (1);
				} else if (currentObject.name == "3rdParty2") {
					spotlight.GetComponent<Light> ().cullingMask = third2Layer;
					showButton (2);
				} else if (currentObject.name == "3rdParty3") {
					spotlight.GetComponent<Light> ().cullingMask = third3Layer;
					showButton (3);
				} else if (currentObject.name == "3rdParty4") {
					spotlight.GetComponent<Light> ().cullingMask = third4Layer;
					showButton (4);
				} else if (currentObject.name == "3rdParty5") {
					spotlight.GetComponent<Light> ().cullingMask = third5Layer;
					//spotlightSwitch (third5Layer);
					showButton (5);
				} else if (currentObject.name == "3rdParty6") {
					spotlight.GetComponent<Light> ().cullingMask = third6Layer;
					showButton (6);
				}

				//runs the A animation of tile expanding
				currentObject.GetComponent<Animator> ().SetTrigger ("hoverOn");
				currentObject.GetComponent<Animator> ().ResetTrigger ("hoverOff");
				oldTile = currentObject;
			}
			//else if a tile is not hovered on, run the B animation that shrinks it
		} else {
			if (currentObject.GetComponent<Animator> () != null && currentObject.tag == "Hoverable") {
				if (!openMenu) {
					currentObject.GetComponent<Animator> ().SetTrigger ("hoverOff");
					currentObject.GetComponent<Animator> ().ResetTrigger ("hoverOn");
					currentTile = currentObject;
					buttonColor = currentButton.GetComponent<Image> ().color;
					currentButton.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
					currentButton.transform.GetChild (0).transform.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
					currentButton.SetActive (false);
					print ("Shrinking " + currentObject.name.ToString ());
				}

			}
		}


		//this is the submenu Animator controller, which does not have an effect on the audio 
		//input project because the submenu is not accessible in this version of the launcher animation
		if (subMenuHoveredOn && openMenu) {
			currentObject.GetComponent<Animator> ().SetTrigger ("menuDwellHoverOn");
			currentObject.GetComponent<Animator> ().ResetTrigger ("menuDwellHoverOff");
			currentTile = currentObject;
			if (currentObject.GetComponent<Animator> () != null && currentObject.tag == "Hoverable") {
				if (currentObject.name == "MenuOption1") {
					showMenuButton (0);
				} else if (currentObject.name == "MenuOption2") {
					showMenuButton (1);
				} else if (currentObject.name == "MenuOption4"){
					showMenuButton (2);
				}
			}
		} else if (!subMenuHoveredOn && openMenu && oldObject.tag != "Selectable"){
			if (currentObject.GetComponent<Animator> () != null) {
				currentObject.GetComponent<Animator> ().SetTrigger ("menuDwellHoverOff");
				currentObject.GetComponent<Animator> ().ResetTrigger ("menuDwellHoverOn");
			}
		}

		//this controls the active state of the submenu colliders, which does not have an effect on the audio 
		//input project because the submenu is not accessible in this version of the launcher animation
		if (openMenu) {
			buttonColliderArray = buttons.GetComponentsInChildren<BoxCollider> ();
			foreach (BoxCollider buttonCollider in buttonColliderArray) {
				buttonCollider.isTrigger = true;
			}
			tileColliderArray = tiles.GetComponentsInChildren<BoxCollider> ();
			foreach (BoxCollider tileCollider in tileColliderArray) {
				tileCollider.isTrigger = true;
			}
		} else {
			buttonColliderArray = buttons.GetComponentsInChildren<BoxCollider> ();
			foreach (BoxCollider buttonCollider in buttonColliderArray) {
				buttonCollider.isTrigger = false;
			}
			tileColliderArray = tiles.GetComponentsInChildren<BoxCollider> ();
			foreach (BoxCollider tileCollider in tileColliderArray) {
				tileCollider.isTrigger = false;
			}
		}

		//this activates/deactivates the colliders of the main menu tiles depending if it is scrolled 
		//up or down
		if (scrolledUp && !openMenu) {
			for (int i = 9; i < 15; i++) {
				tiles.transform.GetChild (i).gameObject.GetComponent<BoxCollider> ().enabled = false;
			}
			for (int i = 0; i < 6; i++) {
				tiles.transform.GetChild (i).gameObject.GetComponent<BoxCollider> ().enabled = true;
			}
		} else {
			for (int i = 9; i < 15; i++) {
				tiles.transform.GetChild (i).gameObject.GetComponent<BoxCollider> ().enabled = true;
			}
			for (int i = 0; i < 6; i++) {
				tiles.transform.GetChild (i).gameObject.GetComponent<BoxCollider> ().enabled = false;
			}
		}
			
		//this closes and deactivates several gameobjects/animations if the reticle is not hovered on an interactable object. 
		if (reticleFollower.getIsInItem() == false) {
			print ("should be minimizing"); 
			currentTile.GetComponent<Animator> ().SetTrigger ("hoverOff");
			currentTile.GetComponent<Animator> ().ResetTrigger ("hoverOn");
			buttonColor = currentButton.GetComponent<Image> ().color;
			currentButton.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
			currentButton.transform.GetChild (0).transform.GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 0.0f);
			currentButton.SetActive (false);
			currentButton.SetActive(false);
			oldButton.SetActive(false);
			oldMenuButton.SetActive (false);
		}

		//this determines what the text of the pop up window will be. the openmenu bool is referring to the submenu, 
		//which is not accesible in the audio input version of this project, to it will always be false
		if (!openMenu) {
			if (currentObject.transform.childCount > 1) {
				currentText = currentObject.transform.GetChild (1).GetComponent<Text> ().text;
				print ("CURRENT TEXT::: " + currentText);
			} else {
				print ("NO CHILD");
			}
		}

		//sets the old object to the current object and rechecks at the top of the update loop
		oldObject = currentObject;
		if (currentButton != null){
			oldButton = currentButton;
		}
		if (currentMenuButton != null) {
			oldMenuButton = currentMenuButton;
		}
	}



	//sets if the launcher is minimized or maximized, which controls the maxim/minim animations
	public void setIsMinimized(bool input){
		isMinimized = input;
	}

	//sets if a main menu tile is hovered on
	public void setTileIsHoveredOn(bool input){
		tileIsHoveredOn = input;
	}

	//sets if the main menu is scrolled up or down
	public void setScrolledUp(bool input){
		scrolledUp = input;
	}

	//sets if the submenu is open or closed
	public void setOpenMenu(bool input){
		openMenu = input;
	}

	//sets if a submenu tile is hovered on
	public void setSubMenuHoveredOn(bool input){
		subMenuHoveredOn = input;
	}

	//shows the corresponding button to whatever main menu tile is hovered on
	private void showButton(int input){
		buttons.transform.GetChild (input).gameObject.SetActive (true);
		currentButton = buttons.transform.GetChild (input).gameObject;

	}

	//shows the corresponding button to whatever submenu tile is hovered on
	private void showMenuButton(int input){
		menuButtons.transform.GetChild (input).gameObject.SetActive (true);
		currentMenuButton = menuButtons.transform.GetChild (input).gameObject;
	}

	//hides the corresponding button to whatever main menu tile is hovered on
	public void hideMenuButton (int input){
			menuButtons.transform.GetChild (input).gameObject.SetActive (false);
	}

	//returns if the main menu is scrolled up or down
	public bool getScrolledUp(){
		return scrolledUp;
	}
		
	//quits
	public void QuitApplication(){
		Application.Quit ();
	}

	//not used in the audio input version of this project
	public void EnableQuads(){
		for (int i = 0; i < quads.Length; i++) {
			quads [i].GetComponent<RawImage>().enabled = true;
		}
	}

	//changes the text on the application window
	public void ShowAppWindow(){
		popUpTitle.GetComponent<Text> ().text = currentText;
	}
		
	//swaps the 'mic selectable' image with the 'mic on' image
	public void micOn(bool input){
		if (input == true) {
			micButton.sprite = audioOn;
		} else {
			micButton.sprite = audioSelectable;
		}
	}


}
