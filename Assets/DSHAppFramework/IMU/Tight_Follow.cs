﻿using UnityEngine;
using System.Collections;

public class Tight_Follow : MonoBehaviour {

	void Start () {
		Behavior_Follow response = gameObject.GetComponent<Behavior_Follow>() ?? gameObject.AddComponent<Behavior_Follow> ();
		response.MaxSecondsOffScreen = 0.0f;
		response.shouldWaitForIMU = false;
		response.shouldTightFollow = true;

	}
}
