﻿using UnityEngine;
using System.Collections;

public class Timed_Loose_Follow : MonoBehaviour {

	// MaxSecondsOffScreen 2.0f, shouldWaitForIMU false
	void Start () {
		Behavior_Follow response = gameObject.GetComponent<Behavior_Follow>() ?? gameObject.AddComponent<Behavior_Follow> ();
		response.MaxSecondsOffScreen = 2.0f;
		response.shouldWaitForIMU = false;
	}
}
