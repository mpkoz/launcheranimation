﻿using UnityEngine;
using System.Collections;

public class IMU_Loose_Follow : MonoBehaviour {

	void Start () {
		Behavior_Follow response = gameObject.GetComponent<Behavior_Follow>() ?? gameObject.AddComponent<Behavior_Follow> ();
		response.MaxSecondsOffScreen = 0.3f;
		response.shouldWaitForIMU = true;
	}

}
