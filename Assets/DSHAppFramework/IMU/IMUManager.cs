﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IMUManager : MonoBehaviour {

	public TextMesh debugText;
	void Start () {

		deltaX = 0.0f;
		deltaY = 0.0f;
		deltaZ = 0.0f;

		DSHUnityPlugin.StartIMU ();
		Debug.Log ("Starting IMU");
		if (debugText != null) {
			debugText.text = "starting IMU";
		}
	}

	static float factorY = 1.0f;

	public static float deltaX {
		get;
		private set;
	}
	public static float deltaY {
		get;
		private set;
	}
	public static float deltaZ {
		get;
		private set;
	}

	public static Vector3 delta {
		get {
			return new Vector3 (deltaX, deltaY , deltaZ);
		}
		private set {
		}
	}

	public static float eulerX {
		get;
		private set;
	}
	public static float eulerY {
		get;
		private set;
	}
	public static float eulerZ {
		get;
		private set;
	}
	
	public static Vector3 euler {
		get {
			return new Vector3 (eulerX, eulerY, eulerZ);
		}
		private set {
		}
	}

	//TODO: Better name
	public static Vector3 deltaSansRoll {
		get {
			return new Vector3 (deltaX, deltaY  , 0.0f);
		}
		private set {
		}
	}

	public static float Round(float value, int digits){
		float mult = Mathf.Pow(10.0f, (float)digits);
		return Mathf.Round(value * mult) / mult;
	}

//	public static Vector3 FilterIMU(float X, float Y, float Z) {
//		float X1 = 0.0f;
//		float Y1 = 0.0f;
//		float Z1 = 0.0f;
//		float threshold = 0.1f;
//
//		//TODO: Check with Naveen about this logic
//		if(Mathf.Abs(Round(X,4)) != 0.0175f && Mathf.Abs(Round(X,4)) != 0.0349f && Mathf.Abs(X) > threshold) {
//			X1 = X;
//		}
//		
//		if(Mathf.Abs(Round(Y,4)) != 0.0175f && Mathf.Abs(Round(Y,4)) != 0.0349f && Mathf.Abs(Y) > threshold) {
//			Y1 = Y;
//		}
//		
//		if(Mathf.Abs(Round(Z,4)) != 0.0175f && Mathf.Abs(Round(Z,4)) != 0.0349f && Mathf.Abs(Z) > threshold) {
//			Z1 = Z;
//		}
//
//		return new Vector3 (X1, Y1, Z1);
//	
//	}


	void Stop () {
		Debug.Log ("Stopping IMU");
		DSHUnityPlugin.StopIMU ();
	}


	void Update () {

		if (Input.GetKeyDown (KeyCode.UpArrow)) 
			IMUManager.factorY += 0.1f;

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			IMUManager.factorY -= 0.1f;

		}

		if (Input.GetMouseButtonDown (0)){
			IMUManager.factorY += 0.1f;
		}

		if (Input.GetMouseButtonDown(1)){
			IMUManager.factorY -= 0.1f;
		}

		if (debugText != null) {

			debugText.text = "Factor = " + IMUManager.factorY;
		}

		#if UNITY_EDITOR
		if(Input.GetMouseButton(0))
		{
			deltaX += Input.GetAxis("Mouse Y") / 5f;
			deltaY -= Input.GetAxis("Mouse X") / 5f;
		}
		deltaX *= 0.9f;
		deltaY *= 0.9f;
		#endif

		IMUData data;
		
		if (DSHUnityPlugin.GetIMUData (out data)) {
			deltaX = data.Gyro.x;
			deltaY = -data.Gyro.z;
			deltaZ = data.Gyro.y;

//			debugText.text = data.Gyro.x.ToString() + " ||| " +
//				data.Gyro.y.ToString() + " ||| " +
//					data.Gyro.z.ToString() ;
			if (debugText != null) {

				debugText.text = "Factor = " + IMUManager.factorY;
			}
			//deltaX = data.Eul.x;
			//deltaX = data.Eul.y;
			//deltaX = data.Eul.z;

			float fFactor = 1.0f;
			
			//Vector3 IMUdelta =  FilterIMU(deltaX,deltaY,deltaZ);
			
//			deltaX = IMUdelta.x;
//			deltaY = IMUdelta.y;
//			deltaZ = IMUdelta.z;
			
//			eulerX += deltaX * fFactor;
//			eulerY += deltaY * fFactor;
//			eulerZ += deltaZ * fFactor;
		} else {
			Debug.LogWarning("IMU data invalid");
		}
	}


}
