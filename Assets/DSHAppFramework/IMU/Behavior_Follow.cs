﻿using UnityEngine;
using System.Collections;

public class Behavior_Follow : MonoBehaviour {
	public bool screenEdgeContent = false;
	public float MaxSecondsOffScreen = 2.0f;
	public bool shouldWaitForIMU = false;
	public bool shouldTightFollow = false;
	protected Vector3 IMUDelta = new Vector3 (0.0f, 0.0f, 0.0f);

	protected bool[] shouldAnimate; 
	protected float[] timeOffScreen;
	protected float[] timeReturnToScreen;
	protected float[] animationLength;
	protected int arraylength;
	protected Quaternion[] LastOffScreenPosition;
	protected Quaternion[] LastOnScreenPosition;

	// Use this for initialization
	void Start () {
		IMUDelta = new Vector3 (0.0f, 0.0f, 0.0f);

		arraylength = this.transform.childCount;
		shouldAnimate = new bool[arraylength];
		timeOffScreen = new float[arraylength];
		timeReturnToScreen = new float[arraylength];
		animationLength = new float[arraylength];
		LastOffScreenPosition = new Quaternion[arraylength];
		LastOnScreenPosition = new Quaternion[arraylength];

		for(int i = 0; i < arraylength; i++){
			shouldAnimate[i] = false;
			timeOffScreen[i] = 0;
			timeReturnToScreen[i] = 0;
			animationLength[i] =  1.0f;
			LastOffScreenPosition[i] = Quaternion.identity;
			LastOnScreenPosition[i] = Quaternion.identity;

		}

	}

	bool isOnScreen(Vector3 pos){

		Vector3 screenpos = Camera.main.WorldToViewportPoint (pos);
		//Debug.Log ("X: " + screenpos.x + " Y: " + screenpos.y);
		if (screenEdgeContent) {
			if (screenpos.x >= -1.75 && screenpos.x <= 2.3 && screenpos.y >= -1.25 && screenpos.y <= 1.25) {
				return true;
			} else {
				return false;
			}
		} else {
			if (screenpos.x >= -1.25 && screenpos.x <= 2.05 && screenpos.y >= -0.5 && screenpos.y <= 1.3) {
				return true;
			} else {
				return false;
			}
		}
	}

	// Update is called once per frame
	void Update () {

		// loop through the spaces
		for(int i = 0; i < arraylength; i++){

			// get the spaces transform
			Transform child = this.gameObject.transform.GetChild(i);

			// add the rotation delta to the spaces
			if(shouldTightFollow){

				// Tight Follow
				child.eulerAngles += IMUManager.deltaSansRoll;
				// don't let the content leave the screen for Tight Follow Behavior
				Transform content = child.GetChild(0);
				if ( isOnScreen(content.transform.position) == false ){
					child.eulerAngles -= IMUManager.deltaSansRoll;
				}
			} else {

				// Loose Follow
				child.eulerAngles += IMUManager.deltaSansRoll;

				//Debug.Log(IMUManager.deltaSansRoll);

			}

			if (shouldAnimate[i]) {

				// animate back onto the screen
				timeReturnToScreen[i] += Time.deltaTime / animationLength[i];
				child.transform.rotation = Quaternion.Slerp (LastOffScreenPosition[i], Quaternion.identity, timeReturnToScreen[i]);

				// check if the animation is finished
				if (timeReturnToScreen[i] >= animationLength[i]) {
					shouldAnimate[i] = false;
					timeReturnToScreen[i] = 0;
				}
			} else {

				// calculate if onscreen
				Transform content = child.GetChild(0);

				if ( isOnScreen(content.transform.position) ){
					// the object is on screen
					timeOffScreen[i] = 0;
				} else {

					// the object is off screen
					if(shouldWaitForIMU){
						// check if IMU activity is low and then bring the content back
						IMUDelta = IMUManager.delta;
						float threshold = 0.2f;

						if(Mathf.Abs(IMUDelta.x) < threshold && Mathf.Abs(IMUDelta.x) < threshold && Mathf.Abs(IMUDelta.z) < threshold){
							timeOffScreen[i] += Time.deltaTime;
							LastOffScreenPosition[i] = child.transform.rotation;
						} else {
							// reset timer if IMU motion is high, even if content is offscreen
							timeOffScreen[i] = 0;
						}

					} else {
						// the object is off screen
						timeOffScreen[i] += Time.deltaTime;
						LastOffScreenPosition[i] = child.transform.rotation;
					}
				}
			}

			// start the animation
			if (timeOffScreen[i] > MaxSecondsOffScreen) {
				shouldAnimate[i] = true;
				timeOffScreen[i] = 0;
			}
		}

	}
}
