﻿using UnityEngine;
using System.Collections;

public class Fixed_To_Body : MonoBehaviour {
	
	protected int arraylength;
	
	// Use this for initialization
	void Start () {
		arraylength = this.transform.childCount;
		DSHUnityPlugin.StartIMU ();
	}


	void Stop () {
		Debug.Log ("Stopping IMU");
		DSHUnityPlugin.StopIMU ();
	}

	// Update is called once per frame
	void Update () {
		// loop through the spaces
		for (int i = 0; i < arraylength; i++) {
			// get the spaces transform
			Transform child = this.gameObject.transform.GetChild (i);
			//child.eulerAngles += IMUManager.deltaSansRoll;
			IMUData data;
			if (DSHUnityPlugin.GetIMUData (out data)) {
				child.rotation = data.Quat;
			}

		}
	}
}
