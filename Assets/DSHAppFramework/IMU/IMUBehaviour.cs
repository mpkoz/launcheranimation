﻿using UnityEngine;
using System.Collections;

public class IMUBehaviour : MonoBehaviour {

	private bool shouldAnimate = false;
	private float timeReturnToScreen = 0.0f;
	private Quaternion lastOffScreenPosition = Quaternion.identity;
	private float thresholdAngle = 0.2f;
	private float thresholdDelay = 3.0f;
	private float timeSinceStationary = 0.0f;


	void Start () {
		DSHUnityPlugin.StartIMU ();
	}
	
	void Stop () {
		Debug.Log ("Stopping IMU");
		DSHUnityPlugin.StopIMU ();
	}
	
	void OnApplicationQuit() {
		Stop ();
	}
	
	// Update is called once per frame
	void Update () {
		IMUData data;
		
		if (DSHUnityPlugin.GetIMUData (out data)) {
			//Debug.Log ("IMU gyro Data -- " + data.Gyro);
			//Debug.Log ("IMU eul Data -- " + data.Eul);
			//transform.localRotation = data.Quat;
			transform.eulerAngles += new Vector3(-data.Gyro.x, data.Gyro.z, 0);

			if (shouldAnimate) {
				
				// animate back onto the screen
				timeReturnToScreen += Time.deltaTime;
				transform.rotation = Quaternion.Slerp (lastOffScreenPosition, Quaternion.identity, timeReturnToScreen);
				
				// check if the animation is finished
				if (timeReturnToScreen >= 1.0f) {
					shouldAnimate = false;
					timeReturnToScreen = 0.0f;
				}
			}
			
			if(Mathf.Abs(data.Gyro.x) < thresholdAngle && Mathf.Abs(data.Gyro.y) < thresholdAngle && Mathf.Abs(data.Gyro.z) < thresholdAngle){
				timeSinceStationary += Time.deltaTime;
				lastOffScreenPosition = transform.rotation;
			} else {
				// reset timer if IMU motion is high, even if content is offscreen
				timeSinceStationary = 0;
			}

			// start the animation
			if (timeSinceStationary > thresholdDelay) {
				shouldAnimate = true;
				timeSinceStationary = 0;
			}


		} else {
			Debug.LogWarning("IMU data invalid");
		}



	}
}
