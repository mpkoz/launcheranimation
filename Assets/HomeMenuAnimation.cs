﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HomeMenuAnimation : MonoBehaviour {

	public GameObject title;
	public GameObject background;
	public GameObject tileList;
	public GameObject reticleFollower;
	public GameObject menuButton;
	public GameObject homeMenu;
	public GameObject titleButton;

	private bool first;
	private bool second;


	// Use this for initialization
	void Start () {
		first = true;
		second = true;
	}

	// Update is called once per frame
	void Update () {

		if (reticleFollower.GetComponent<ReticleFollower> ().getIsInItem () == false) {
			//if (first) {
				this.GetComponent<Animator> ().SetTrigger ("isExit");
				first = false;
				second = true;
			//}

			//print ("isExited" + this.GetComponent<Animator> ().GetBool ("isExited").ToString ());
			if (this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("HomeMenuMinimize") &&
		    this.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).normalizedTime >= 1.0f) {
				title.SetActive (false);
				background.SetActive (false);
				tileList.SetActive (false);
				homeMenu.SetActive (false);
				//menuButton.SetActive (true);
				titleButton.SetActive (false);

				print ("INSIDEEEEEE");
			}
		}else{
			
			//if (second) {
				this.GetComponent<Animator> ().SetTrigger ("isEnter");
				second = false;
				first = true;
			//}
			title.SetActive (true);
			if (title.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("TitleSlide") &&
			    title.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).normalizedTime >= 1.0f) {
				background.SetActive (true);
				titleButton.SetActive (true);
				if (background.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("BackgroundFade") &&
				    background.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).normalizedTime >= 1.0f) {
					tileList.SetActive (true);
					if (tileList.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("MenuSlide") &&
					    tileList.GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).normalizedTime >= 1.0f) {
						print ("HI");
					}
				}
			}
		}
	}
}
