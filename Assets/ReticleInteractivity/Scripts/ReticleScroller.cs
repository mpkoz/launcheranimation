using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReticleScroller : MonoBehaviour {

	public enum ScrollDirection
	{
		SCROOL_UP,
		SCROOL_DOWN
	};
	
	public ScrollRect _scrollRest;
	public ScrollDirection _scrollDirection = ScrollDirection.SCROOL_UP;
	public float speed = 5.5f;
	private float _speed;

	public bool _isEnabledScrolling = false;

	// Use this for initialization
	void Start () {
		_isEnabledScrolling = false;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(_isEnabledScrolling)
		{
			GameObject content = _scrollRest.transform.GetChild (0).gameObject;
			float height = content.GetComponent<RectTransform> ().rect.height;
			_speed = speed / height;

			Debug.Log(height);
			Debug.Log(_scrollRest.verticalNormalizedPosition);
			if(_scrollDirection == ScrollDirection.SCROOL_UP)
			{
				ScrollUp ();
			}else
			{
				ScrollDown();
			}
		}

	}

	void ScrollUp ()
	{
		if(_scrollRest.verticalNormalizedPosition > 0.0f)
			_scrollRest.verticalNormalizedPosition -= _speed;
	}

	void ScrollDown()
	{
		if(_scrollRest.verticalNormalizedPosition < 1.0f)
			_scrollRest.verticalNormalizedPosition += _speed;
	}
	
	void OnEnable()
	{
		ReticleFollower.Instance.ReticleEnteredSelectableItem += OnReticleEntered;
		
		ReticleFollower.Instance.ReticleExitedSelectableItem += OnReticleExited;

	}
	
	void OnReticleEntered(Transform hitObject)
	{
		if(hitObject.name == transform.name)
		{
			_isEnabledScrolling = true;
		}
	}
	
	void OnReticleExited()
	{
		_isEnabledScrolling = false;
	}
}
