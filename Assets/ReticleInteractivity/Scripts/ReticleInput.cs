﻿using UnityEngine;
using System.Collections;

public class ReticleInput : MonoBehaviour {

	private ReticleRaycastState mCurrentState;

	internal void ChangeRaycastHitState(ReticleRaycastState newState)
	{
		if(mCurrentState != null)
		{
			mCurrentState.ExitState();
		}
		
		mCurrentState = newState;
		mCurrentState.EnterState();

	}

	void Update()
	{
		//mCurrentState.UpdateState ();

	}

	public class ReticleRaycastState
	{
		public ReticleRaycastState(ReticleInput parent) {
			m_parent = parent;
		}
		
		protected ReticleInput parent
		{
			get {
				return m_parent;
			}
		}
		
		virtual public void EnterState() {
		}
		
		virtual public void UpdateState() {
		}
		
		virtual public void ExitState() {}
		
		virtual public bool NextState() {
			return false;
		}
		
		virtual public void HandleRaycastEntered() {
		}
		
		virtual public void HandleRaycastExited() {
		}
		
		protected ReticleInput m_parent;
		
	}

	internal class HoverButton : ReticleRaycastState
	{
		
		public HoverButton(ReticleInput parent) : base(parent) {}
		
		
		override public void HandleRaycastEntered() 
		{

		}

		override public void HandleRaycastExited() 
		{
			
		}
		
		override public void EnterState() 
		{
			
		}
		
		override public void ExitState() 
		{	

		}
		
		override public void UpdateState()
		{

		}

	}



}
