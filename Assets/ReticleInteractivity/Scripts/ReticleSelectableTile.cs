﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider))]

public class ReticleSelectableTile : MonoBehaviour {

	public UnityEvent functionsToCall;

	void OnEnable () {
		//TimerSpriteAnimator.SpriteDidAnimate += OnItemSelected;
	}

	void OnDisable() {
		//TimerSpriteAnimator.SpriteDidAnimate -= OnItemSelected;
	}
	
	void OnItemSelected(Transform hitObject)
	{
		if (hitObject.Equals(this.transform)) {
			functionsToCall.Invoke ();
			Debug.Log ("Activated " + transform.name + " at " + Time.time);
		}
	}

	public void Invoke(){
		functionsToCall.Invoke ();
	}

}
