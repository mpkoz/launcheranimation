﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TimerSpriteAnimator : MonoBehaviour {

	public List<Sprite> m_sprites;

	public float m_delay;

	public static event System.Action<Transform> SpriteDidAnimate;

	void Start()
	{
		m_spriteRenderer = GetComponent<SpriteRenderer> ();
		m_audioSource = GetComponent<AudioSource> ();

		ReticleFollower.Instance.ReticleEnteredSelectableItem += OnReticleEnteredSelectableItem;
		ReticleFollower.Instance.ReticleExitedSelectableItem += OnReticleExitedSelectableItem;
	}

	void OnDestory()
	{
		ReticleFollower.Instance.ReticleEnteredSelectableItem -= OnReticleEnteredSelectableItem;
		ReticleFollower.Instance.ReticleExitedSelectableItem -= OnReticleExitedSelectableItem;
	}

	public void OnReticleEnteredSelectableItem(Transform hitObject)
	{
		StartCoroutine (LoopThroughSprites (hitObject));
	}

	private void OnReticleExitedSelectableItem()
	{
		m_exit = true;
	}

	private IEnumerator LoopThroughSprites(Transform hitObject)
	{
		
		m_delay = hitObject.gameObject.GetComponent<ReticleSelectableItem> ().GetDwellTime();
		m_exit = false;
		float waitTime = m_delay / m_sprites.Count;

		if (hitObject.gameObject.tag == "Selectable") {
			m_audioSource.Play ();
			foreach (Sprite sprite in m_sprites) {
				if (m_exit) {
					break;
				}
				m_spriteRenderer.sprite = sprite;

				yield return new WaitForSeconds (waitTime);

				if (!m_audioSource.isPlaying)
					m_audioSource.Play ();
			}

			GetComponent<AudioSource> ().Stop ();
			m_spriteRenderer.sprite = null;
			Debug.Log ("Animation Over at " + Time.time);
		}

		if (!m_exit && TimerSpriteAnimator.SpriteDidAnimate != null) {
				TimerSpriteAnimator.SpriteDidAnimate (hitObject);
		}
		

	}

	void Update(){

		item = reticle.GetComponent<ReticleFollower> ().getItem ();

		if (item != null) {
			inMenu = true;

			buttonPosition = reticle.GetComponent<ReticleFollower> ().getButtonPosition ();
			//print (spacebar.name);

			if (item.gameObject != spacebar) {
				//this.transform.localScale = new Vector3 (item.localScale.x / 6.0f, item.localScale.y / 6.0f, item.localScale.z / 6.0f);
			} else {

				reticlePosition = new Vector3 (buttonPosition.x, buttonPosition.y, buttonPosition.z); 
				this.transform.position = reticlePosition;
				this.transform.localScale = new Vector3 (0.18f, 0.18f, 0.18f);

			}
		} else {
			inMenu = false;
		}


	}

	public bool getIsInMenu(){
		return inMenu;
	}

	private SpriteRenderer m_spriteRenderer;
	private bool m_playSound;
	private AudioSource m_audioSource;
	private bool m_exit;
	public GameObject reticle;
	private Vector3 buttonPosition;
	private Vector3 reticlePosition;
	public GameObject spacebar;
	private Transform item;
	private bool inMenu;
}
