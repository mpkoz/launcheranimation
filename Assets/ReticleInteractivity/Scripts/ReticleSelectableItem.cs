﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider))]

public class ReticleSelectableItem : MonoBehaviour {


	public GameObject menuLogicObject;

	public UnityEvent functionsToCall;
	private float dwellTime = 0.0f;

	void OnEnable () {
		SetDwellTime();
		TimerSpriteAnimator.SpriteDidAnimate += OnItemSelected;
	}

	void OnDisable() {
		TimerSpriteAnimator.SpriteDidAnimate -= OnItemSelected;
	}
	
	void OnItemSelected(Transform hitObject)
	{
		if (hitObject.Equals(this.transform)) {
			functionsToCall.Invoke ();
			Debug.Log ("Activated " + transform.name + " at " + Time.time);
//			if (gameObject.GetComponent<Button> ()) {
//				gameObject.GetComponent<Button> ().onClick.Invoke();
//			}
		}
	}

	public void Invoke(){
		functionsToCall.Invoke ();
	}

	public void SetDwellTime(){
		switch(this.gameObject.tag){
		case "Selectable":
			dwellTime = 0.55f;
			break;

		case "Hoverable":
			dwellTime = 0.55f;
			break;
		}
	}

	public float GetDwellTime(){
		return dwellTime;
	}



}
