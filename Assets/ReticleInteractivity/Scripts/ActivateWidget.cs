﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Events;

public class ActivateWidget : MonoBehaviour {

	public List<GameObject> widgetsToActivateNext;
	public List<GameObject> widgetsToDeactivate;

	void OnActivateButton()
	{
		foreach (GameObject widget in widgetsToActivateNext) {
			if(widget)
			{
				widget.SetActive(true);
			}
		}
		foreach (GameObject widget in widgetsToDeactivate) {
			if (widget) {
				widget.SetActive (false);
			}
		}

	}
}
