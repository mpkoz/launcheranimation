﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Button))]
public class ButtonInteractivity : MonoBehaviour {

	private Button button
	{
		get {
			if(_button == null) {
				_button = GetComponent<Button>();
			}
			return _button;
		}
	}

	private Button _button;

	ColorBlock colorBlock;
	Color normalColor;
	Color highlightedColor;
	public UnityEvent InvokeReticleEntered;
	public UnityEvent InvokeReticleExited;


	void Start()
	{
		ReticleFollower.Instance.ReticleEnteredHoverableItem += OnReticleEntered;
		ReticleFollower.Instance.ReticleExitedHoverableItem += OnReticleExited;
	}

	void OnDestory()
	{
		ReticleFollower.Instance.ReticleEnteredHoverableItem -= OnReticleEntered;
		ReticleFollower.Instance.ReticleExitedHoverableItem -= OnReticleExited;
	}



	void OnEnable()
	{
		ReticleFollower.Instance.ReticleEnteredSelectableItem += OnReticleEntered;
		ReticleFollower.Instance.ReticleExitedSelectableItem += OnReticleExited;

		colorBlock = button.colors;
		normalColor = button.colors.normalColor;
		highlightedColor = button.colors.highlightedColor;
	}

	void OnDisable()
	{
		OnReticleExited ();

		ReticleFollower.Instance.ReticleEnteredSelectableItem -= OnReticleEntered;
		ReticleFollower.Instance.ReticleExitedSelectableItem -= OnReticleExited;
	}

	void SwapColor()
	{
		Color temp = colorBlock.normalColor;
		colorBlock.normalColor = colorBlock.highlightedColor;
		colorBlock.highlightedColor = temp;
		button.colors = colorBlock;
	}

	void OnReticleEntered(Transform hitObject)
	{
		if(hitObject.Equals(this.transform))
		{
			//print ("entered");
			colorBlock.normalColor = highlightedColor;
			colorBlock.highlightedColor = normalColor;
			button.colors = colorBlock;

			InvokeReticleEntered.Invoke();
		}
	}

	void OnReticleExited()
	{
		//print ("exited");
		colorBlock.normalColor = normalColor;
		colorBlock.highlightedColor = highlightedColor;
		button.colors = colorBlock;

		InvokeReticleExited.Invoke ();
	}
}