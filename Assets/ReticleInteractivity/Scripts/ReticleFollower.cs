using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ReticleState { RELEASE, ATTACH };

public class ReticleFollower : MonoBehaviour {

	public event System.Action<Transform> ReticleEnteredSelectableItem;
	public event System.Action ReticleExitedSelectableItem;

	public event System.Action<Transform> ReticleEnteredHoverableItem;
	public event System.Action ReticleExitedHoverableItem;

	public static ReticleFollower Instance
	{
		get{
			if(m_instance == null)
			{
				m_instance = GameObject.FindObjectOfType(typeof(ReticleFollower)) as ReticleFollower;
			}
			return m_instance;
		}
	}

	void Start()
	{
		m_camera = transform.parent.GetComponent<Camera> ();
		contentPosition = content.GetComponent<ContentPosition> ();
		isMoving = false;
		oldObject = this.gameObject;
		startTime = Time.time;
		timeCheck = false;
	}

	void Update()
	{
		Vector3 dir;
		if (m_camera != null) {
			dir = (transform.position - m_camera.transform.position).normalized;
			m_ray = new Ray (transform.position, dir);
		} else {
			Vector3 rayorigin = new Vector3 (0.0f, this.transform.position.y, 0.0f);
			m_ray = new Ray (rayorigin, this.transform.forward);
		}

		isMoving = contentPosition.IsMoving;

		if (isInItem == true) {
			
			//InvokeRepeating("ExitEnterSelectable", 2, 100.0f);

		}
			


		if (Physics.Raycast (m_ray, out m_hit, 10000)) {

			item = m_hit.transform;
			currentObject = item.gameObject;
			//print (item.gameObject.name);
			isInItem = true;

			if (item.gameObject.tag == "Selectable") {

				hovering = true;
				buttonPosition = item.transform.position;
				//Debug.Log ("S: "+m_rayEnteredSelectableItem.ToString());
				if (!m_rayEnteredSelectableItem && !isMoving) {
					m_rayEnteredSelectableItem = true;
					m_rayEnteredHoverableItem = false;

					if (this.ReticleEnteredSelectableItem != null && item != null && !isMoving) {
						currentObject = item.gameObject;
						this.ReticleEnteredSelectableItem (item);
					}

				}
			} else if (item.gameObject.tag == "Hoverable") {
				//currentObject = item.gameObject;
				hovering = true;
				if (this.ReticleExitedSelectableItem != null && m_rayEnteredSelectableItem) {
					this.ReticleExitedSelectableItem ();
				}
				//Debug.Log ("H: "+m_rayEnteredHoverableItem.ToString());
				m_rayEnteredSelectableItem = false;
				if (!m_rayEnteredHoverableItem) {
					m_rayEnteredSelectableItem = false;
					m_rayEnteredHoverableItem = true;
					//Debug.Log ("over hover");
					if (this.ReticleEnteredHoverableItem != null) {
						this.ReticleEnteredHoverableItem (item);
					}
				}


			}
		} else {
			//print ("not hitting something");
			isInItem = false;
			hovering = false;
			if (m_rayEnteredSelectableItem) {
				m_rayEnteredSelectableItem = false;

				if (this.ReticleExitedSelectableItem != null) {
					this.ReticleExitedSelectableItem ();
					currentObject = null;
				}
			}

			if (m_rayEnteredHoverableItem) {
				m_rayEnteredHoverableItem = false;

				if (this.ReticleExitedHoverableItem != null) {
					this.ReticleExitedHoverableItem ();
					currentObject = null;
				}
			}
		}

		oldObject = currentObject;
		//print ("isInItem: " + isInItem);

	}

	public Vector3 getButtonPosition(){
		return buttonPosition;
	}

	public Transform getItem(){
		return item;
	}

	public bool getHovering(){
		return hovering;
	}

	public bool getCurrentObject(){
		return currentObject;
	}

	public IEnumerator Wait(){
		yield return new WaitForSeconds (2.0f);

	}

	public bool getIsInItem(){
		return isInItem;
	}

	public void ExitEnterSelectable(){

		this.ReticleExitedSelectableItem ();
		this.ReticleEnteredSelectableItem (item);
		print("did it");
		CancelInvoke ();
	}




	private static ReticleFollower m_instance;
	private ReticleState m_currState;
	private Camera m_camera;
	private bool m_rayEnteredHoverableItem;
	private bool m_rayEnteredSelectableItem;
	private RaycastHit m_hit;
	private Ray m_ray;
	private Vector3 buttonPosition;
	private bool hovering;
	private GameObject currentObject;
	private GameObject oldObject;
	private Transform item;
	private Transform oldItem;
	private bool timeCheck;
	private float startTime;


	public GameObject content;
	private ContentPosition contentPosition;
	private bool isMoving;
	private bool isInItem;

}





